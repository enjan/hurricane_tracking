#!/usr/bin/env python

import numpy as np


def timeupdate(year, month, day, hour, minute, second, inc):

    # convert to int
    intsecond = int(second) + inc
    intminute = int(minute)
    inthour = int(hour)
    intday = int(day)
    intmonth = int(month)
    intyear = int(year)

    # leapyear
    leapyear = 0
    if np.mod(intyear, 4):
        leapyear = 1
    if np.mod(intyear, 100):
        leapyear = 0
    if np.mod(intyear, 400):
        leapyear = 1

    # second
    while intsecond >= 60:
        intsecond = intsecond - 60
        intminute = intminute + 1
    # minute
    while intminute >= 60:
        intminute = intminute - 60
        inthour = inthour + 1
    # hour
    while inthour >= 24:
        inthour = inthour - 24
        intday = intday + 1
    # day
    if (
        intmonth == 1
        or intmonth == 3
        or intmonth == 5
        or intmonth == 7
        or intmonth == 8
        or intmonth == 10
        or intmonth == 12
    ):
        while intday > 31:
            intday = intday - 31
            intmonth = intmonth + 1
    if intmonth == 4 or intmonth == 6 or intmonth == 9 or intmonth == 11:
        while intday > 30:
            intday = intday - 30
            intmonth = intmonth + 1
    if intmonth == 2:
        if leapyear == 1:
            while intday > 29:
                intday = intday - 29
                intmonth = intmonth + 1
        if leapyear == 0:
            while intday > 28:
                intday = intday - 28
                intmonth = intmonth + 1
    # month
    while intmonth > 12:
        intmonth = intmonth - 12
        intyear = intyear + 1

    # convert to string
    if intsecond < 10:
        second = "0" + str(intsecond)
    else:
        second = str(intsecond)

    if intminute < 10:
        minute = "0" + str(intminute)
    else:
        minute = str(intminute)

    if inthour < 10:
        hour = "0" + str(inthour)
    else:
        hour = str(inthour)

    if intday < 10:
        day = "0" + str(intday)
    else:
        day = str(intday)

    if intmonth < 10:
        month = "0" + str(intmonth)
    else:
        month = str(intmonth)
    year = str(intyear)

    return year, month, day, hour, minute, second
