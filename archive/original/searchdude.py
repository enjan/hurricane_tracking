#!/usr/bin/env python

import numpy as np
import math


def searchtc(slp, vor, tem, hgt, slpdis, maxlat, vormin, temdif, temdis, res, lon, lat):
    print("searching for TCs")
    ni = slp.shape[2]
    nj = slp.shape[1]
    tcloc = np.ones((nj, ni))

    tcloc = slpmin(slp, tcloc, hgt, ni, nj, res, slpdis)
    print("sum slploc: " + str(np.sum(tcloc)))

    #  tcloc = latmax(tcloc,ni,nj,maxlat)
    #  print('sum latmax: '+str(np.sum(tcloc)))

    tcloc = vorval(vor, tcloc, ni, nj, vormin)
    print("sum vorloc: " + str(np.sum(tcloc)))

    tclonin, tclatin = temmax(tem, tcloc, res, ni, nj, temdif, temdis, lon, lat)
    print("sum temmax: " + str(np.sum(tcloc)))

    return tclonin, tclatin


# --------------------------------------------------------------


def slpmin(slp, tcloc, hgt, ni, nj, res, slpdis):

    print("### slpmin")

    # determine number of indices of search box side length
    mpdeg = 2 * np.pi * 6371229 / 360
    nind = round(slpdis / mpdeg * (1 / res))
    print("nind:  " + str(nind))

    for i in range(0, ni):
        for j in range(0, nj):
            # check if height is positive
            if math.isnan(hgt[49, j, i]):
                tcloc[j, i] = 0
                continue

            # check if slp is above minimum threshold
            if slp[0, j, i] < 88000:
                tcloc[j, i] = 0
                continue
            # check if slp is below maximum threshold
            if slp[0, j, i] > 101000:
                tcloc[j, i] = 0

            for ii in range(-nind, nind + 1):
                for jj in range(-nind, nind + 1):
                    if ii == 0 and jj == 0:
                        continue

                    indi = i + ii
                    indj = j + jj

                    # adjust for out of bounds indices
                    if indi < 0:
                        indi = indi + ni
                    if indi >= ni:
                        indi = indi - ni
                    if indj < 0:
                        indj = indj + nj
                    if indj >= nj:
                        indj = indj - nj

                    # check for minimum
                    if slp[0, j, i] > slp[0, indj, indi]:
                        tcloc[j, i] = 0

    return tcloc


# --------------------------------------------------------------


def latmax(tcloc, ni, nj, latmax):

    print("### latmax")

    # find index of lat cutoffs

    inpd = nj / 180  # indices per degree
    latmaxin = int(inpd * latmax)  # distance of latmax to eq in deg
    northin = int(latmaxin + nj / 2)
    southin = int(nj / 2 - latmaxin)

    tcloc[0:southin, :] = 0
    tcloc[northin:nj, :] = 0

    return tcloc


# --------------------------------------------------------------


def vorval(vor, tcloc, ni, nj, vormin):

    print("### vorval")

    for i in range(0, ni):
        for j in range(0, nj):
            if tcloc[j, i] == 0:
                continue
            if vor[0, 32, j, i] < vormin:
                tcloc[j, i] = 0

    return tcloc


# --------------------------------------------------------------


def temmax(tem, tcloc, res, ni, nj, temdif, temdis, lon, lat):

    print("### temmax")

    # check how many indices away to search
    mpdeg = 2 * np.pi * 6371229 / 360
    nind = round(temdis / mpdeg * (1 / res))
    print("nind:  " + str(nind))

    n = 0
    tclonin = np.array([0])
    tclatin = np.array([0])

    for i in range(0, ni):
        for j in range(0, nj):
            if tcloc[j, i] == 1:
                t = 0

                ii = i + nind
                if ii >= ni:
                    ii = ii - nj
                if tem[0, 0, j, i] >= tem[0, 0, j, ii] + temdif:
                    t = t + 1

                ii = i - nind
                if ii < 0:
                    ii = ii + ni
                if tem[0, 0, j, i] >= tem[0, 0, j, ii] + temdif:
                    t = t + 1

                jj = j + nind
                if jj >= nj:
                    jj = jj - nj
                if tem[0, 0, j, i] >= tem[0, 0, jj, i] + temdif:
                    t = t + 1

                jj = j - nind
                if jj < 0:
                    jj = jj + nj
                if tem[0, 0, j, i] >= tem[0, 0, jj, i] + temdif:
                    t = t + 1

                if t < 4:
                    tcloc[j, i] = 0

                if tcloc[j, i] == 1:
                    print("lon: ", lon[i])
                    print("lat: ", lat[j])
                    n = n + 1
                    tclonin = np.append(tclonin, i)
                    tclatin = np.append(tclatin, j)

    tclonin[0] = n
    tclatin[0] = n

    return tclonin, tclatin
