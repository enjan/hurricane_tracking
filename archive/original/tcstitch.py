#!/usr/bin/env python

import numpy as np
from timeupdate import timeupdate
import os

# sharpen the mix
member = "10"
exp = "25_40"
inc = 3600 * 6
spd = 20  # maximum speed of a tc in m/s
minlength = 3  # minimum lifetime in timesteps
r_e = 6371229  # radius of Earth

# to all kinds of trix
infname = "../out/TC_" + member + "_" + exp + ".txt"
outname = "../out/IDTC_" + member + "_" + exp + ".txt"

endtimes = False  # end of file reached
ripperoni = False  # end of t file reached
infile = open(infname, "r")

maxd = spd * inc  # maximum distance between two timesteps

n = 0
ID = 1
aID = [ID]  # active IDs

while not endtimes:
    # read line
    dat = infile.readline()
    dat = dat.split()

    try:
        datetime = dat[0]
    except:
        break

    print("---------------------------------")
    print(datetime)

    # for first file
    if n == 0:
        # save relevant data
        dID = [1]
        lonID = [dat[3]]
        latID = [dat[4]]

        # open new file and write first line
        file = open("../t/" + str(ID) + ".txt", "w")
        file.write(
            str(ID)
            + "\t"
            + dat[0]
            + "\t"
            + dat[1]
            + "\t"
            + dat[2]
            + "\t"
            + dat[3]
            + "\t"
            + dat[4]
            + "\t"
            + dat[5]
            + "\t"
            + dat[6]
        )
        file.close()

    # for following files
    else:
        # check datetime
        if olddatetime != datetime:
            year, month, day, hour, minute, second = timeupdate(
                olddatetime[0:4],
                olddatetime[5:7],
                olddatetime[8:10],
                olddatetime[11:13],
                olddatetime[14:16],
                olddatetime[17:19],
                inc,
            )
            udatetime = (
                year
                + "-"
                + month
                + "-"
                + day
                + "T"
                + hour
                + ":"
                + minute
                + ":"
                + second
                + "Z"
            )

        if udatetime != datetime:
            # check all active IDs for length
            for i in aID:
                ind = aID.index(i)
                if dID[ind] < minlength:
                    #          wolololo = 1
                    os.remove("../t/" + str(i) + ".txt")

            # remove all active IDs if timesteps are skipped and create new ID
            ID = ID + 1
            aID = [ID]
            dID = [1]
            lonID = [dat[3]]
            latID = [dat[4]]
            file = open("../t/" + str(ID) + ".txt", "a")
            file.write(
                str(ID)
                + "\t"
                + dat[0]
                + "\t"
                + dat[1]
                + "\t"
                + dat[2]
                + "\t"
                + dat[3]
                + "\t"
                + dat[4]
                + "\t"
                + dat[5]
                + "\t"
                + dat[6]
            )
            file.close()

        else:
            new = True
            # check location
            datlon = float(dat[3]) * np.pi / 180.0
            datlat = float(dat[4]) * np.pi / 180.0
            for i in aID:
                print(aID)
                ind = aID.index(i)
                print(ind)
                IDlat = float(latID[ind]) * np.pi / 180.0
                IDlon = float(lonID[ind]) * np.pi / 180.0
                d = r_e * np.arccos(
                    np.sin(IDlat) * np.sin(datlat)
                    + np.cos(IDlat) * np.cos(datlat) * np.cos(IDlon - datlon)
                )
                if d <= maxd:
                    file = open("../t/" + str(i) + ".txt", "a")
                    file.write("\n")
                    file.write(
                        str(i)
                        + "\t"
                        + dat[0]
                        + "\t"
                        + dat[1]
                        + "\t"
                        + dat[2]
                        + "\t"
                        + dat[3]
                        + "\t"
                        + dat[4]
                        + "\t"
                        + dat[5]
                        + "\t"
                        + dat[6]
                    )
                    file.close()
                    lonID[ind] = dat[3]
                    latID[ind] = dat[4]
                    dID[ind] = dID[ind] + 1
                    new = False
                    continue
            # if location is not within a previous TC, create new ID
            if new:
                ID = ID + 1
                aID.append(ID)
                dID.append(1)
                lonID.append(dat[3])
                latID.append(dat[4])
                file = open("../t/" + str(ID) + ".txt", "a")
                file.write(
                    str(ID)
                    + "\t"
                    + dat[0]
                    + "\t"
                    + dat[1]
                    + "\t"
                    + dat[2]
                    + "\t"
                    + dat[3]
                    + "\t"
                    + dat[4]
                    + "\t"
                    + dat[5]
                    + "\t"
                    + dat[6]
                )
                file.close()

    # replace datetime
    olddatetime = datetime

    n = n + 1

# merge files
outfile = open(outname, "w")
for f in range(1, ID + 1):
    try:
        file = open("../t/" + str(f) + ".txt")
    except:
        continue

    while not ripperoni:
        # read line
        dat = file.readline()
        dat = dat.split()

        try:
            datetime = dat[0]
        except:
            file.close()
            os.remove("../t/" + str(f) + ".txt")
            break

        outfile.write(
            dat[0]
            + "\t"
            + dat[1]
            + "\t"
            + dat[2]
            + "\t"
            + dat[3]
            + "\t"
            + dat[4]
            + "\t"
            + dat[5]
            + "\t"
            + dat[6]
            + "\t"
            + dat[7]
        )
        outfile.write("\n")

outfile.close()


print("done")
