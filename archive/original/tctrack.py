#!/usr/bin/env python

import numpy as np
from timeupdate import timeupdate
from searchdude import searchtc
from datadude import readfile
from datadude import writetc
from winddude import windmax
import matplotlib.pyplot as plt

# sharpen the mix
initime = "2013-06-01T000000Z"
endtime = "2013-12-01T000000Z"
inc = 3600 * 6  # time increment in seconds

member = "07"
exp = "25_40"
indir = "/wolke_scratch/enzb/ICON/output/sim_13k/2013_" + member + "_" + exp + "/remap/"
indir2 = (
    "/wolke_scratch/enzb/ICON/output/sim_13k/2013_" + member + "_" + exp + "/remap/"
)
inbase = "REMAP_NWP_LAM_DOM01_"
inbase2 = "REMAP_NWP_LAM_PL_T_DOM01_"
outname = "TC_" + member + "_" + exp + ".txt"

slpdis = 50000  # minimum distance between two slp minima
maxlat = 45  # maximum latitude that is considered in degrees
vormin = 10 ** (-5)  # minimum vorticity to pass as TC
temdif = 1  # temperature difference of warm core
temdis = 200000  # distance at which location is considered "environment"
winddis = 100000  # distance within which to look for wind maximum
maxhgt = 3000  # maximum height where mind maximum is searched
res = 0.125  # resolution in degrees

# to all kinds of trix
year = initime[0:4]
month = initime[5:7]
day = initime[8:10]
hour = initime[11:13]
minute = initime[13:15]
second = initime[15:17]

s = 0
dowhile = 1

outfile = open(r"../out/" + outname, "w")

# loop over time
while dowhile == 1:

    # update time
    if s > 0:
        year, month, day, hour, minute, second = timeupdate(
            year, month, day, hour, minute, second, inc
        )
    if (
        year == endtime[0:4]
        and month == endtime[5:7]
        and day == endtime[8:10]
        and hour == endtime[11:13]
        and minute == endtime[13:15]
        and second == endtime[15:17]
    ):
        dowhile = 0

    print("######################################")
    if s < 10:
        print(
            "### "
            + year
            + "-"
            + month
            + "-"
            + day
            + "T"
            + hour
            + minute
            + second
            + "Z"
            + "   step: "
            + str(s)
            + "   ###"
        )
    elif s < 100:
        print(
            "### "
            + year
            + "-"
            + month
            + "-"
            + day
            + "T"
            + hour
            + minute
            + second
            + "Z"
            + "   step: "
            + str(s)
            + "  ###"
        )
    else:
        print(
            "### "
            + year
            + "-"
            + month
            + "-"
            + day
            + "T"
            + hour
            + minute
            + second
            + "Z"
            + "   step: "
            + str(s)
            + " ###"
        )
    print("######################################")

    # read data
    slp, vor, u, v, tem, hgt, lon, lat = readfile(
        indir, indir2, inbase, inbase2, year, month, day, hour, minute, second
    )

    # search for TCs
    tclonin, tclatin = searchtc(
        slp, vor, tem, hgt, slpdis, maxlat, vormin, temdif, temdis, res, lon, lat
    )

    # skip to next iteration if no TCs found
    if tclonin[0] >= 1:
        # determine maximum wind
        maxwind = windmax(u, v, tclonin, tclatin, hgt, maxhgt, winddis)

        # write to text file
        writetc(
            year,
            month,
            day,
            hour,
            minute,
            second,
            lon,
            lat,
            tclonin,
            tclatin,
            slp,
            maxwind,
            outfile,
        )

    # update step number
    s = s + 1
    print()

outfile.close()
