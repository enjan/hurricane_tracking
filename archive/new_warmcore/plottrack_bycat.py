#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.basemap import Basemap

# sharpen the mix
members = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10"]
exp = "25_40"

shad = "intensity"
colors = [
    (1, 0, 0),
    (1, 0.3, 0.3),
    (1, 0.6, 0.6),
    (1, 0.9, 0.9),
    (1, 0.12, 0.12),
    (1, 0.15, 0.15),
    (1, 0.18, 0.18),
    (1, 0.21, 0.21),
    (1, 0.24, 0.24),
    (1, 0.27, 0.27),
    (1, 0.3, 0.3),
    (1, 0.33, 0.33),
    (1, 0.36, 0.36),
    (1, 0.39, 0.39),
    (1, 0.42, 0.42),
    (1, 0.45, 0.45),
    (1, 0.48, 0.48),
    (1, 0.51, 0.51),
    (1, 0.54, 0.54),
    (1, 0.57, 0.57),
    (1, 0.6, 0.6),
]
colorl = [
    (0, 0, 0),
    (0, 1, 0),
    (1, 0.9, 0),
    (1, 0.7, 0),
    (1, 0.5, 0),
    (1, 0.3, 0),
    (1, 0.1, 0),
]

cat = [-1, 0, 1, 2, 3, 4, 5]

# to all kinds of trix
endtimes = False
if shad == "lifetime":
    col = colors
elif shad == "intensity":
    col = colorl

# loop over all cats
for cat in cat:

    # open figure with map
    plt.figure(figsize=(12, 8))
    mp = Basemap(
        llcrnrlon=-110.0,
        llcrnrlat=0.0,
        urcrnrlon=20.0,
        urcrnrlat=50.0,
        projection="lcc",
        lat_1=20.0,
        lat_2=40.0,
        lon_0=-60.0,
        resolution="l",
        area_thresh=1000.0,
    )
    mp.drawcoastlines()
    mp.drawparallels(np.arange(10, 70, 20), labels=[1, 1, 0, 0])
    mp.drawmeridians(np.arange(-140, 20, 20), labels=[0, 0, 0, 1])
    plt.title(
        "Hurricane Tracks   Simulation Group "
        + exp
        + "   Shading "
        + shad
        + "   cat "
        + str(cat)
    )

    print("cat:      ", cat)
    print("exp:      ", exp)
    print("shading:  ", shad)

    for mem in range(0, len(members)):

        print("member:  ", members[mem])

        # generate file name
        infile = "../out/catIDTC_" + members[mem] + "_" + exp + ".txt"

        # open file
        file = open(infile, "r")

        # read first line
        dat = file.readline()
        dat = dat.split()
        oldlon = float(dat[5])
        oldlat = float(dat[6])
        ID = int(dat[0])
        ltc = 0

        # loop over all lines
        n = -1
        while not endtimes:

            n = n + 1

            # read line
            dat = file.readline()
            dat = dat.split()

            try:
                newlon = float(dat[5])
                newlat = float(dat[6])
            except:
                break

            # skip if cat is wrong
            if int(float(dat[1])) != int(cat):
                continue

            # check if new ID
            if int(dat[0]) != ID:
                oldlon = float(dat[5])
                oldlat = float(dat[6])
                ID = int(dat[0])
                ltc = 0
                continue

            # determine color
            if shad == "lifetime":
                clr = col[ltc]
            elif shad == "intensity":
                wspd = float(dat[8])
                if wspd < 17:
                    clr = col[0]
                elif wspd < 33:
                    clr = col[1]
                elif wspd < 43:
                    clr = col[2]
                elif wspd < 50:
                    clr = col[3]
                elif wspd < 58:
                    clr = col[4]
                elif wspd < 70:
                    clr = col[5]
                else:
                    clr = col[6]

            # plot line
            lon = [oldlon, newlon]
            lat = [oldlat, newlat]
            x, y = mp(lon, lat)
            mp.plot(x, y, color=clr)
            if ltc < len(col) - 1:
                ltc = ltc + 1

            # pass lon and lat
            oldlon = newlon
            oldlat = newlat

    plt.savefig("../plots/tracks_" + exp + "_" + shad + "_" + "cat" + str(cat) + ".png")
    plt.close()
