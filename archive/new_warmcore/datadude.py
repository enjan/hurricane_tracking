#!/usr/bin/env python


def readfile(indir, indir2, inbase, inbase2, year, month, day, hour, minute, second):

    from netCDF4 import Dataset

    # create file names
    fname = indir + inbase + year + month + day + "T" + hour + minute + second + "Z.nc"
    fname2 = (
        indir2 + inbase2 + year + month + day + "T" + hour + minute + second + "Z.nc"
    )

    print("attempting to read file " + fname)

    dataset = Dataset(fname)
    slp = dataset.variables["pres_msl"][:]
    vor = dataset.variables["vor"][:]
    u = dataset.variables["u"][:]
    v = dataset.variables["v"][:]
    hgt = dataset.variables["z_mc"][:]
    lon = dataset.variables["lon"][:]
    lat = dataset.variables["lat"][:]

    print("attempting to read file " + fname2)

    dataset = Dataset(fname2)
    tem = dataset.variables["temp"][:]

    return slp, vor, u, v, tem, hgt, lon, lat


def writetc(
    year,
    month,
    day,
    hour,
    minute,
    second,
    lon,
    lat,
    tclonin,
    tclatin,
    slp,
    maxwind,
    outfile,
):

    for t in range(0, maxwind.shape[0]):
        outfile.write(
            year
            + "-"
            + month
            + "-"
            + day
            + "T"
            + hour
            + ":"
            + minute
            + ":"
            + second
            + "Z"
        )
        outfile.write("\t")
        outfile.write(str(tclonin[t + 1]))
        outfile.write("\t")
        outfile.write(str(tclatin[t + 1]))
        outfile.write("\t")
        outfile.write(str(lon[tclonin[t + 1]]))
        outfile.write("\t")
        outfile.write(str(lat[tclatin[t + 1]]))
        outfile.write("\t")
        outfile.write(str(slp[0, tclatin[t + 1], tclonin[t + 1]]))
        outfile.write("\t")
        outfile.write(str(maxwind[t]))
        outfile.write("\n")
