#!/usr/bin/env python

import numpy as np


def windmax(u, v, tclonin, tclatin, hgt, maxhgt, winddis):

    print("### windmax")

    # allocate maxwind
    maxwind = np.zeros(int(tclonin[0]))

    # determine maximum height
    maxhgtin = np.argwhere(hgt[:, tclatin[1], tclonin[1]] < maxhgt)
    maxhgtin = np.min(maxhgtin)
    if (hgt[maxhgtin, tclatin[1], tclonin[1]] - maxhgt) < (
        maxhgt - hgt[maxhgtin - 1, tclatin[1], tclonin[1]]
    ):
        maxhgtin = maxhgtin - 1

    # look for max distance over which to search
    mpdeg = 2 * np.pi * 6372339 / 360
    nind = round(winddis / (mpdeg * 180 / hgt.shape[1]))
    print("nind: ", nind)

    # loop over found TCs
    for t in range(1, tclonin[0] + 1):
        # calculate wind magnitude
        utemp = u[
            0,
            0:maxhgtin,
            tclatin[t] - nind : tclatin[t] + nind + 1,
            tclonin[t] - nind : tclonin[t] + nind + 1,
        ]
        vtemp = v[
            0,
            0:maxhgtin,
            tclatin[t] - nind : tclatin[t] + nind + 1,
            tclonin[t] - nind : tclonin[t] + nind + 1,
        ]
        magwind = np.sqrt(utemp * utemp + vtemp * vtemp)

        maxwind[t - 1] = np.amax(magwind)
    print("maximum wind speed: " + str(maxwind[t - 1]))

    return maxwind
