import os

import dask.dataframe as dd
import matplotlib.pyplot as plt
import numpy as np
import qgrid
from scipy.interpolate import griddata
from sklearn.neighbors import DistanceMetric
from tqdm import tqdm

from utils import load_simulation_data, load_complete_datasets

dist = DistanceMetric.get_metric("haversine")

r_e = 6371229

members = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10"]
experiments = ["ref", "rm"]

sim_base_dir = "/wolke_scratch/enzb/ICON/output/sim_13k_largedomain"
inbase = "REMAP_NWP_LAM_DOM01_"
inbase2 = "REMAP_NWP_LAM_PL_T_DOM01_"

outdir = "/home/enjan/hurricane_tracking/results/analysis"

algo_data_file = "/home/enjan/hurricane_tracking/results/analysis/algo_output.hdf5"


def cart2pol(x, y):
    theta = np.arctan2(y, x)
    rho = np.hypot(x, y)
    return theta, rho


def get_tc_genesis(df):
    return df.sort_values("date").groupby("tc_id").first()


def get_coordinates(dataset):
    """
    Get coordinate Systems
    :param dataset: netcdf4 Dataset of simulation data
    :return: different types of coordinates
    (561,)          lon
    (841,)          lat
    (561, 841)      latlat
    (561, 841)      lonlon
    (471801, 2)     latlon
    """
    lon = dataset.variables["lon"][:]
    lat = dataset.variables["lat"][:]
    latlat, lonlon = np.meshgrid(lat, lon, indexing="ij")
    latlon = np.vstack([latlat.ravel(), lonlon.ravel()]).T
    return lat, lon, latlat, lonlon, latlon


def crop(array, row, ravel=False, dist=40):
    xdim = array.ndim - 2
    ydim = array.ndim - 1
    top = max(row.lat_idx - dist, 0)
    bottom = min(row.lat_idx + dist, array.shape[xdim])
    left = max(row.lon_idx - dist, 0)
    right = min(row.lon_idx + dist, array.shape[ydim])

    if array.ndim == 2:
        data = array[
            top:bottom,
            left:right,
        ]
    elif array.ndim == 3:
        data = array[
            :,
            top:bottom,
            left:right,
        ]
    elif array.ndim == 4:
        data = array[
            0,
            :,
            top:bottom,
            left:right,
        ]
    else:
        raise (f"Dont't know how to deal with {array.data.ndims} dimensions!")
    if ravel:
        return data.ravel()
    else:
        return data


def get_qgrid(pdf):
    qgrid_widget = qgrid.show_grid(pdf.astype("category"), show_toolbar=True)
    return qgrid_widget


def calc_radial_and_tangential_velocity(
    tc_data, theta, do_crop=False, crop_params=None
):
    v = tc_data["v"][0, :]
    u = tc_data["u"][0, :]
    if do_crop:
        v = crop(v, **crop_params)
        u = crop(u, **crop_params)
        theta = crop(theta, **crop_params)
    radial = v * np.sin(theta) + u * np.cos(theta)
    tangential = v * np.cos(theta) + u * np.sin(theta)
    return radial, tangential


def interpolate_data3d(sim_data, row, zz, yy, xx, res=(100, 500, 500), dist=40):
    cyy, cxx = (
        crop(np.tile(yy, (50, 1, 1)), row, ravel=True, dist=dist),
        crop(np.tile(xx, (50, 1, 1)), row, ravel=True, dist=dist),
    )
    czz = crop(zz, row, ravel=True, dist=dist)
    values = crop(sim_data, row, dist=dist)
    points = np.vstack([czz, cyy, cxx]).T
    zgrid = np.linspace(czz.min(), czz.max(), res[0])
    ygrid = np.linspace(cyy.min(), cyy.max(), res[1])
    xgrid = np.linspace(cxx.min(), cxx.max(), res[2])
    czz, cxx, cyy = np.meshgrid(zgrid, ygrid, xgrid, indexing="ij")
    grid = np.vstack([czz.ravel(), cxx.ravel(), cyy.ravel()]).T
    print("start interpolation")
    print(values.shape, points.shape, grid.shape)
    data = griddata(points, values.ravel(), grid, method="linear").reshape(res)
    return grid, data


def interpolate_data(sim_data, row, res, latlat, lonlon, dist=40):
    values = crop(sim_data, row, dist=dist)
    clatlat, clonlon = (
        crop(latlat, row, ravel=True, dist=dist),
        crop(lonlon, row, ravel=True, dist=dist),
    )
    points = np.vstack([clatlat, clonlon]).T
    latgrid, longrid = (
        np.linspace(clatlat.min(), clatlat.max(), res[0]),
        np.linspace(clonlon.min(), clonlon.max(), res[1]),
    )
    cxx, cyy = np.meshgrid(latgrid, longrid, indexing="ij")
    grid = np.vstack([cxx.ravel(), cyy.ravel()]).T
    data = np.empty((sim_data.shape[0], *res))
    for level in tqdm(range(0, sim_data.shape[0])):
        print(points.shape, values[level].ravel().shape)
        data[level, :, :] = griddata(
            points, values[level].ravel(), grid, method="linear", rescale=True
        ).reshape(res)
    return grid, data


def calculate_azimean(data, row, grid, res, mean_resolution=200):
    dr = 1000
    drh = dr / 2.0
    tc_np = np.array([row.lat, row.lon]).reshape((1, 2))
    R = (r_e * dist.pairwise(np.deg2rad(tc_np), np.deg2rad(grid))).reshape(res)
    r = np.linspace(R.min() + dr, R.max() - dr, num=mean_resolution)

    azimean = np.empty((data.shape[0], mean_resolution))

    for level in tqdm(range(0, data.shape[0])):
        level_data = data[level, :, :]
        f = lambda r: level_data[(R >= r - drh) & (R < r + drh)].mean()
        azimean[level, :] = np.vectorize(f)(r)
    return r, azimean


def calculate_cartesian_azimean(data, row, grid, res, mean_resolution=200):
    dr = 1000
    drh = dr / 2.0
    tc_np = np.array([row.lat, row.lon]).reshape((1, 2))
    R = np.sqrt(grid[:, 0] ** 2 + grid[:, 1] ** 2).reshape(res)
    r = np.linspace(R.min() + dr, R.max() - dr, num=mean_resolution)

    azimean = np.empty((50, mean_resolution))

    for level in tqdm(range(0, 50)):
        level_data = data[level, :, :]
        f = lambda r: level_data[(R >= r - drh) & (R < r + drh)].mean()
        azimean[level, :] = np.vectorize(f)(r)
    return r, azimean


def get_plot_filename(row, data_type):
    return f"tc{row.tc_id:06}-{data_type}-{row.date.strftime('%Y-%m-%d-T%H')}.eps"


def plot_azimean(r, z, azimean, data_type, row):
    if data_type == "tangential":
        title = f"tangential wind   vmax = {np.amax(azimean)}"
        xlabel = "radius [km]"
        ylabel = "height [km]"
        levels = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70]
        cmap = "Reds"
    elif data_type == "radial":
        title = "radial wind"
        xlabel = "radius [km]"
        ylabel = "height [km]"
        levels = [-12, -10, -8, -6, -4, -2, 0, 2, 4, 6, 8, 10, 12]
        cmap = "bwr"
    elif data_type == "vertical":
        title = "vertical wind"
        xlabel = "radius [km]"
        ylabel = "height [m]"
        levels = np.arange(-1, 1.1, 0.1)
        cmap = "bwr"
    fig, ax = plt.subplots(1, 1, figsize=(12, 8))
    axf = ax.contourf(r / 1000, z / 1000, azimean, levels=levels, cmap=cmap)
    fig.colorbar(axf)
    ax.set_title(title)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    fname = get_plot_filename(row, data_type)
    plt.savefig(fname)
    plt.show()


def create_tc_plots(row, dist=40, interpolation_shape=(1000, 1000), mean_resolution=50):
    d1, d2 = load_complete_datasets(row)
    lat, lon, latlat, lonlon, latlon = get_coordinates(d1)
    theta = calculate_theta_around_tc(row, latlon).reshape(latlat.shape)
    radial, tangential = calc_radial_and_tangential_velocity(d1.variables, theta)

    grid_vert, data_vert = interpolate_data(
        d1.variables["w"][0, :], row, interpolation_shape, latlat, lonlon, dist=dist
    )

    grid_rad, data_rad = interpolate_data(
        radial, row, interpolation_shape, latlat, lonlon, dist=dist
    )
    grid_tan, data_tan = interpolate_data(
        tangential, row, interpolation_shape, latlat, lonlon, dist=dist
    )

    r_vert, azimean_vert = calculate_azimean(
        data_vert, row, grid_vert, interpolation_shape, mean_resolution=mean_resolution
    )

    r_rad, azimean_rad = calculate_azimean(
        data_rad,
        row,
        grid_rad,
        interpolation_shape,
        mean_resolution=mean_resolution,
    )
    r_tan, azimean_tan = calculate_azimean(
        data_tan,
        row,
        grid_tan,
        interpolation_shape,
        mean_resolution=mean_resolution,
    )
    plot_azimean(
        r_rad,
        d1.variables["z_mc"][:, row.lat_idx, row.lon_idx],
        azimean_rad,
        "radial",
        row,
    )
    plot_azimean(
        r_tan,
        d1.variables["z_mc"][:, row.lat_idx, row.lon_idx],
        azimean_tan,
        "tangential",
        row,
    )
    plot_azimean(
        r_vert,
        d1.variables["z_ifc"][:, row.lat_idx, row.lon_idx],
        azimean_vert,
        "vertical",
        row,
    )
    d1.close(), d2.close()


def calculate_distance_between_points(points1, points2):
    return r_e * dist.pairwise(np.deg2rad(points1), np.deg2rad(points2))


def calculate_distance_around_tc(row, points):
    tc_np = np.array([row.lat, row.lon]).reshape((1, 2))
    return calculate_distance_between_points(tc_np, points)


def calculate_theta_between_points(pointsA, pointsB):
    """
    Calculate theta between 1 point and a list of points or between pairs of points
    :param pointsA: (n, 2) or (1, 2) array
    :param points: (n, 2) or (m, 2) array
    :return: (n,) or (m,) array
    """
    lat1 = np.deg2rad(pointsA[:, 0])
    lat2 = np.deg2rad(pointsB[:, 0])

    diffLong = np.deg2rad(pointsB[:, 1] - pointsA[:, 1])

    x = np.sin(diffLong) * np.cos(lat2)
    y = np.cos(lat1) * np.sin(lat2) - (np.sin(lat1) * np.cos(lat2) * np.cos(diffLong))

    initial_bearing = np.arctan2(x, y)
    data = (-initial_bearing + 2.0 * np.pi) % (2 * np.pi)
    data = (data + 0.5 * np.pi) % (2 * np.pi)

    return data


def calculate_theta_around_tc(row, points):
    tc_np = np.array([row.lat, row.lon]).reshape((1, 2))
    return calculate_theta_between_points(tc_np, points)


def plot_theta(theta, row):
    plt.figure(dpi=75)
    plt.imshow(theta, origin="lower")
    plt.colorbar()
    plt.plot(row.lon_idx, row.lat_idx, "ro")
    plt.title("Theta in respect to TC")
    plt.show()


def plot_radius(radius, row):
    plt.figure(dpi=75)
    plt.imshow(radius, origin="lower")
    plt.colorbar()
    plt.plot(row.lon_idx, row.lat_idx, "ro")
    plt.title("Distance from TC")
    plt.show()


if __name__ == "__main__":
    os.makedirs(outdir, exist_ok=True)

    df = dd.read_hdf(algo_data_file, "df")
    pdf = dd.read_hdf(algo_data_file, "params")

    row = df.head(1).iloc[0]
    tc_data = load_simulation_data(df.head(1).iloc[0])
    d1, d2 = load_complete_datasets(row)
    lat, lon, latlat, lonlon, latlon = get_coordinates(d1)

    theta = calculate_theta_around_tc(row, latlon).reshape(latlat.shape)
    r = calculate_distance_around_tc(row, latlon).reshape(latlat.shape)
    print("jo")

    d1.close()
    d2.close()
