#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
from netCDF4 import Dataset


def weightgen_main(fname, r0, rinc, res, ci, cj, ni, nj):

    # read data
    lon, lat = datread(fname)

    # remap to physical space relative to fixed point
    me, mn = remap(lon, lat, ni, nj)

    # define points
    points = pointdef(r0, rinc, res, ci, cj, me, mn)

    # generate weights
    index, weights = weightgen(points, me, mn)

    # calculate center position
    center = centergen(ci, cj, me, mn)

    return index, weights, points, center, me, mn


# ---------------------------------------------------------------
def remap(lon, lat, ni, nj):

    print("remapping to physical space")

    # allocate me and mn
    me = np.empty([lat.shape[0], lon.shape[0]])
    mn = np.empty([lat.shape[0]])

    # calculate distance from equator
    eqi = np.around(lat.shape[0] / 2)  # equator index
    eqi = eqi.astype(int)
    dwi = np.around(lon.shape[0] / 6)  # degrees west index
    dwi = dwi.astype(int)
    r_earth = 6371229.0
    dpd = 2 * np.pi * r_earth / 360  # distance per degree
    print(lat.shape)
    print(mn.shape)
    for lai in range(0, lat.shape[0]):
        mn[lai] = (lat[lai] - lat[eqi]) * dpd

    # calculate distance from 120 degrees W (index 1000)
    for lai in range(0, lat.shape[0]):
        for loi in range(0, lon.shape[0]):
            me[lai, loi] = (lon[loi] - lon[dwi]) * dpd * np.cos(np.deg2rad(lat[lai]))

    return me, mn


# ---------------------------------------------------------------
def pointdef(r0, rinc, res, ci, cj, me, mn):

    print("defining points for averaging")

    # points in in [east:north, point, radius]

    # allocate array
    npmax = np.around(2 * np.pi * r0 / res)
    npmax = npmax.astype(int)
    points = np.ones([2, npmax, int(r0 / rinc + 1)]) * -999999

    # open figure
    fig, ax = plt.subplots(1, 1, figsize=(12, 12))
    plt.xlim(me[cj, ci] - 1.1 * r0, me[cj, ci] + 1.1 * r0)
    plt.ylim(mn[cj] - 1.1 * r0, mn[cj] + 1.1 * r0)
    plt.plot(me[cj, ci], mn[cj], marker="x", markersize=5, color="red")

    # find points
    for r in range(1, points.shape[2]):
        # number of points
        npoint = np.around(2 * np.pi * r * rinc / res)
        npoint = npoint.astype(int)

        # use angle and radius to determine location of points
        for p in range(0, npoint):
            # get angle
            alpha = p * (2 * np.pi / npoint)

            # calculate offset in both horizontal directions
            xoff = r * rinc * np.cos(alpha)
            yoff = r * rinc * np.sin(alpha)

            # calculate location relative to reference frame
            points[0, p, r] = xoff + me[cj, ci]
            points[1, p, r] = yoff + mn[cj]

            # add marker to figure
            plt.plot(
                points[0, p, r],
                points[1, p, r],
                marker="x",
                markersize=5,
                color="black",
            )

    print("saving plot of point positions as ../out/pointposition.png")
    plt.savefig("../out/pointposition.png")
    plt.close("all")

    return points


# ---------------------------------------------------------------
def weightgen(points, me, mn):

    # every point (p,r) has distance to east and north in points,
    # indices to ne/nw/sw/se in index, and
    # weights for each index in weights, as ne/nw, se/sw, n/s weights

    print("generating indices and weights")

    index = np.zeros([2, 4, points.shape[1], points.shape[2]])
    weights = np.zeros([3, points.shape[1], points.shape[2]])

    # loop over all points
    for p in range(0, points.shape[1]):
        for r in range(1, points.shape[2]):
            if points[0, p, r] != -999999:
                # look for index to north and south
                index[1, 0, p, r] = np.min(np.where(mn > points[1, p, r]))
                index[1, 1, p, r] = index[1, 0, p, r]
                index[1, 2, p, r] = index[1, 0, p, r] - 1
                index[1, 3, p, r] = index[1, 2, p, r]

                # look for index to east to north and south
                index[0, 0, p, r] = np.min(
                    np.where(me[int(index[1, 0, p, r]), :] > points[0, p, r])
                )
                index[0, 1, p, r] = index[0, 0, p, r] - 1
                index[0, 3, p, r] = np.min(
                    np.where(me[int(index[1, 2, p, r]), :] > points[0, p, r])
                )
                index[0, 2, p, r] = index[0, 3, p, r] - 1

                # generate weights
                # zonal, north side
                weights[0, p, r] = (
                    me[int(index[1, 0, p, r]), int(index[0, 0, p, r])] - points[0, p, r]
                ) / (
                    me[int(index[1, 0, p, r]), int(index[0, 0, p, r])]
                    - me[int(index[1, 1, p, r]), int(index[0, 1, p, r])]
                )
                # zonal, south side
                weights[1, p, r] = (
                    me[int(index[1, 3, p, r]), int(index[0, 3, p, r])] - points[0, p, r]
                ) / (
                    me[int(index[1, 3, p, r]), int(index[0, 3, p, r])]
                    - me[int(index[1, 2, p, r]), int(index[0, 2, p, r])]
                )
                # meridional
                weights[2, p, r] = (mn[int(index[1, 0, p, r])] - points[1, p, r]) / (
                    mn[int(index[1, 0, p, r])] - mn[int(index[1, 2, p, r])]
                )

    return index, weights


# ---------------------------------------------------------------
def centergen(ci, cj, me, mn):

    center = [me[cj, ci], mn[cj]]

    return center


# ---------------------------------------------------------------
def datread(fname):

    print("reading lon/lat data from ", fname)

    dataset = Dataset(fname)

    lon = dataset.variables["lon"][:]
    lat = dataset.variables["lat"][:]

    return lon, lat
