#!/usr/bin/enzb python

import numpy as np
from netCDF4 import Dataset


def datload(infile, ni, nj, nk, f):

    dataset = Dataset(infile)

    if f == 0 or f == 1:
        dat = np.empty([2, nk, nj, ni])
        dat[0, :, :, :] = dataset.variables["u"][:]
        dat[1, :, :, :] = dataset.variables["v"][:]

    if f == 2:
        dat = dataset.variables["w"][:]
    if f == 5:
        dat = dataset.variables["vor"][:]
    if f == 6:
        dat = dataset.variables["temp"][:]
    if f == 3:
        dat = dataset.variables["pres_msl"][:]
    if f == 4:
        dat = dataset.variables["lhfl_s"][:]

    return dat
