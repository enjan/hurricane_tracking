#!/usr/bin/env python

import numpy as np
from datread import datload
from netCDF4 import Dataset
from weightgen import weightgen_main
from azmeancalc import azmeancalc_main
from plotazimean import plotazimean_main

# import matplotlib.pyplot as plt

# sharpen the mix
field = np.empty(7)
field[0] = 1  # tangential wind
field[1] = 1  # radial wind
field[2] = 1  # vertical wind
field[3] = 1  # surface pressure
field[4] = 0  # latent heat flux
field[5] = 1  # vorticity
field[6] = 1  # temperature difference

ci = 381  # lon of tc center
cj = 182  # lat of tc center
ni = 804
nj = 434
nk = 50

# tc_id 99554

year = "2013"
month = "09"
day = "05"
hour = "12"
minute = "00"
second = "00"

exp = "ref"
member = "02"
ID = "99554"
fdir = (
    "/wolke_scratch/enzb/ICON/output/sim_13k_largedomain/2013_"
    + member
    + "_"
    + exp
    + "/remap/"
)
fbase = "REMAP_NWP_LAM_DOM01_"

r0 = 500000  # outermost radius
rinc = 5000  # increment in radius
res = 6500  # resolution of simulation

# to all kinds of trix
fname = fbase + year + month + day + "T" + hour + minute + second + "Z.nc"
fname = fdir + fname
radplot = np.zeros(int(r0 / rinc) + 1)
for r in range(0, radplot.shape[0]):
    radplot[r] = r * rinc
ind_rmw = (0, 0)

# generate points and weights
index, weights, points, center, me, mn = weightgen_main(
    fname, r0, rinc, res, ci, cj, ni, nj
)

# generate and plot azimuthal mean data
for f in range(0, field.shape[0]):
    if field[f] == 1:
        # create azimuthal means
        meanfield, height, ind_rmw = azmeancalc_main(
            fname,
            field,
            f,
            index,
            weights,
            points,
            center,
            ci,
            cj,
            rinc,
            res,
            me,
            mn,
            ind_rmw,
        )

        # plot fields
        plotazimean_main(
            meanfield,
            height,
            radplot,
            member,
            ID,
            year,
            month,
            day,
            hour,
            minute,
            second,
            f,
            ind_rmw,
        )
