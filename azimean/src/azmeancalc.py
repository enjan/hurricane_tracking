#!/usr/bin/env python

from netCDF4 import Dataset
import numpy as np


def azmeancalc_main(
    fname, field, f, index, weights, points, center, ci, cj, rinc, res, me, mn, ind_rmw
):

    # read data
    data, height = datread(fname, field, f, ci, cj, center, rinc, res, me, mn)

    # interpolate to points
    if f == 3 or f == 4:
        azmean = meancalc2D(data, index, weights, points, ci, cj)
    elif f == 0 or f == 1:
        azmean = meancalctanrad(data, height, index, weights, points, ci, cj, f)
    else:
        azmean = meancalc3D(data, height, index, weights, points, ci, cj)

    # get temperature differences
    if f == 6:
        azmean = diffcalc(azmean)

    # get RMW
    if ind_rmw == (0, 0):
        ind_rmw = np.unravel_index(azmean.argmax(), azmean.shape)
        print(ind_rmw)

    return azmean, height, ind_rmw


# ---------------------------------------------------------------
def meancalc2D(data, index, weights, points, ci, cj):

    # initialize data
    azmean = np.zeros([weights.shape[2]])
    azmean[0] = data[0, cj, ci]

    for r in range(1, weights.shape[2]):

        n = 0
        val = 0

        for p in range(0, weights.shape[1]):
            if points[0, p, r] != -999999:
                # interpolate to north
                north = (
                    weights[0, p, r]
                    * data[0, int(index[1, 1, p, r]), int(index[0, 1, p, r])]
                    + (1 - weights[0, p, r])
                    * data[0, int(index[1, 0, p, r]), int(index[0, 0, p, r])]
                )
                # interpolate to south
                south = (
                    weights[1, p, r]
                    * data[0, int(index[1, 2, p, r]), int(index[0, 2, p, r])]
                    + (1 - weights[1, p, r])
                    * data[0, int(index[1, 3, p, r]), int(index[0, 3, p, r])]
                )
                # combine
                n = n + 1
                val = val + weights[2, p, r] * south + (1 - weights[2, p, r]) * north

        # average
        azmean[r] = val / n

    return azmean


# ---------------------------------------------------------------
def meancalctanrad(data, height, index, weights, points, ci, cj, f):

    # initialize data
    azmean = np.zeros([weights.shape[2], data.shape[2]])
    north = np.zeros([2, data.shape[2]])
    south = np.zeros([2, data.shape[2]])
    val = np.zeros([2, weights.shape[1], data.shape[2]])
    tan = np.zeros([data.shape[2]])
    rad = np.zeros([data.shape[2]])

    for r in range(1, weights.shape[2]):
        #  for r in range(1,2):
        n = 0
        val[:, :, :] = 0
        tan[:] = 0
        rad[:] = 0

        for p in range(0, weights.shape[1]):
            if points[0, p, r] != -999999:
                for h in range(0, height.shape[0]):
                    # interpolate to north
                    north[0, h] = (
                        weights[0, p, r]
                        * data[0, 0, h, int(index[1, 1, p, r]), int(index[0, 1, p, r])]
                        + (1 - weights[0, p, r])
                        * data[0, 0, h, int(index[1, 0, p, r]), int(index[0, 0, p, r])]
                    )
                    north[1, h] = (
                        weights[0, p, r]
                        * data[1, 0, h, int(index[1, 1, p, r]), int(index[0, 1, p, r])]
                        + (1 - weights[0, p, r])
                        * data[1, 0, h, int(index[1, 0, p, r]), int(index[0, 0, p, r])]
                    )

                    # interpolate to south
                    south[0, h] = (
                        weights[1, p, r]
                        * data[0, 0, h, int(index[1, 2, p, r]), int(index[0, 2, p, r])]
                        + (1 - weights[1, p, r])
                        * data[0, 0, h, int(index[1, 3, p, r]), int(index[0, 3, p, r])]
                    )
                    south[1, h] = (
                        weights[1, p, r]
                        * data[1, 0, h, int(index[1, 2, p, r]), int(index[0, 2, p, r])]
                        + (1 - weights[1, p, r])
                        * data[1, 0, h, int(index[1, 3, p, r]), int(index[0, 3, p, r])]
                    )

                # combine
                n = n + 1
                for h in range(0, height.shape[0]):
                    val[0, p, h] = (
                        weights[2, p, r] * south[0, h]
                        + (1 - weights[2, p, r]) * north[0, h]
                    )  # u
                    val[1, p, h] = (
                        weights[2, p, r] * south[1, h]
                        + (1 - weights[2, p, r]) * north[1, h]
                    )  # v

        # calculate tanwind and radwind
        for point in range(0, n):
            alpha = point * (2 * np.pi / n)

            for h in range(0, height.shape[0]):
                # partition into radial and tangential
                if f == 0:
                    tan[h] = (
                        tan[h]
                        - val[0, point, h] * np.sin(alpha)
                        + val[1, point, h] * np.cos(alpha)
                    )
                elif f == 1:
                    rad[h] = (
                        rad[h]
                        + val[0, point, h] * np.cos(alpha)
                        + val[1, point, h] * np.sin(alpha)
                    )

        for h in range(0, height.shape[0]):
            # average
            if f == 0:
                azmean[r, h] = tan[h] / n
            if f == 1:
                azmean[r, h] = rad[h] / n

    return azmean


# ---------------------------------------------------------------
def meancalc3D(data, height, index, weights, points, ci, cj):

    # initialize data
    azmean = np.zeros([weights.shape[2], data.shape[1]])
    azmean[0, :] = data[0, :, cj, ci]
    north = np.zeros(data.shape[1])
    south = np.zeros(data.shape[1])
    val = np.zeros(data.shape[1])

    for r in range(1, weights.shape[2]):
        n = 0
        val[:] = 0

        for p in range(0, weights.shape[1]):
            if points[0, p, r] != -999999:
                for h in range(0, height.shape[0]):
                    # interpolate to north
                    north[h] = (
                        weights[0, p, r]
                        * data[0, h, int(index[1, 1, p, r]), int(index[0, 1, p, r])]
                        + (1 - weights[0, p, r])
                        * data[0, h, int(index[1, 0, p, r]), int(index[0, 0, p, r])]
                    )
                    # interpolate to south
                    south[h] = (
                        weights[1, p, r]
                        * data[0, h, int(index[1, 2, p, r]), int(index[0, 2, p, r])]
                        + (1 - weights[1, p, r])
                        * data[0, h, int(index[1, 3, p, r]), int(index[0, 3, p, r])]
                    )
                # combine
                n = n + 1
                for h in range(0, height.shape[0]):
                    val[h] = (
                        val[h]
                        + weights[2, p, r] * south[h]
                        + (1 - weights[2, p, r]) * north[h]
                    )

        for h in range(0, height.shape[0]):
            # average
            azmean[r, h] = val[h] / n

    return azmean


# ---------------------------------------------------------------
def diffcalc(azmean):

    for r in range(0, azmean.shape[0]):
        for h in range(0, azmean.shape[1]):
            azmean[r, h] = azmean[r, h] - azmean[int(azmean.shape[0] - 1), h]

    return azmean


# ---------------------------------------------------------------
def datread(fname, field, f, ci, cj, center, rinc, res, me, mn):

    dataset = Dataset(fname)

    print("reading data to create averages for field " + str(f))
    if f == 0 or f == 1:
        data = np.zeros([2, 1, 50, mn.shape[0], me.shape[1]])
        data[0, :, :, :, :] = dataset.variables["u"][:]
        data[1, :, :, :, :] = dataset.variables["v"][:]
    if f == 2:
        data = dataset.variables["w"][:]
        data = data[:, 0:50, :, :]
    if f == 3:
        data = dataset.variables["pres_msl"][:]
    if f == 4:
        data = dataset.variables["lhfl_s"][:]
    if f == 5:
        data = dataset.variables["vor"][:]
    if f == 6:
        data = dataset.variables["temp"][:]

    # read height data for 3D fields
    if f != 3 and f != 4:
        height = dataset.variables["z_mc"][:]
        height = height[:, cj, ci]
    else:
        height = 0

    return data, height
