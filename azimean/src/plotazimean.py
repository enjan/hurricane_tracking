#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import matplotlib
matplotlib.rcParams.update({'font.size': 16})

def plotazimean_main(
    meanfield,
    height,
    radplot,
    member,
    ID,
    year,
    month,
    day,
    hour,
    minute,
    second,
    f,
    ind_rmw,
):

    # create plot name
    fname, fieldname = fname_gen(f, member, ID, year, month, day, hour, minute, second)

    # create plot
    azimeanplot(f, meanfield, radplot, height, ind_rmw)

    # save figure
    plt.savefig(fname)

    # close figure
    plt.close("all")


# ---------------------------------------------------------------
def azimeanplot(f, meanfield, radplot, height, ind_rmw):

    # figure title and axis labels
    if f == 0:
        title = "tangential wind   vmax = " + str(np.amax(meanfield))
        xlabel = "radius [km]"
        ylabel = "height [m]"
        levels = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70]
        cmap = "Reds"
    elif f == 1:
        title = "radial wind"
        xlabel = "radius [km]"
        ylabel = "height [m]"
        levels = [-12, -10, -8, -6, -4, -2, 0, 2, 4, 6, 8, 10, 12]
        cmap = "bwr"
    elif f == 2:
        title = "vertical wind"
        xlabel = "radius [km]"
        ylabel = "height [m]"
        levels = [
            -1,
            -0.9,
            -0.8,
            -0.7,
            -0.6,
            -0.5,
            -0.4,
            -0.3,
            -0.2,
            -0.1,
            0,
            0.1,
            0.2,
            0.3,
            0.4,
            0.5,
            0.6,
            0.7,
            0.8,
            0.9,
            1,
        ]
        cmap = "bwr"
    elif f == 3:
        title = "surface pressure"
        xlabel = "radius [km]"
        ylabel = "pressure [Pa]"
    elif f == 4:
        title = "latent heat flux"
        xlabel = "radius [km]"
        ylabel = "latent heat flux [W]"
    elif f == 5:
        title = "vorticity"
        xlabel = "radius [km]"
        ylabel = "height [m]"
        levels = [
            -0.00175,
            -0.0015,
            -0.00125,
            -0.001,
            -0.00075,
            -0.00050,
            -0.00025,
            -0,
            0.00025,
            0.00050,
            0.00075,
            0.001,
            0.00125,
            0.0015,
            0.00175,
        ]
        cmap = "bwr"
    elif f == 6:
        title = "temperature difference"
        xlabel = "radius [km]"
        ylabel = "height [m]"
        levels = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        cmap = "Reds"

    fig, ax = plt.subplots(1, 1, figsize=(12, 8),dpi=300)
    if f == 3 or f == 4:
        ax.plot(radplot / 1000, meanfield)
        plt.plot(
            (radplot[ind_rmw[0]] / 1000, radplot[ind_rmw[0]] / 1000), (0, 103000), c="k"
        )
    else:
        meanfield = np.transpose(meanfield)
        axf = ax.contourf(radplot / 1000, height, meanfield, levels=levels, cmap=cmap)
        fig.colorbar(axf)
        plt.plot(radplot[ind_rmw[0]] * np.ones(height.shape) / 1000, height, c="k")

    ax.set_title(title)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    plt.xticks(np.arange(0, 550, 50))
    ax.set_xlim(0, 500)
    if f == 3:
        ax.set_ylim([94000, 102000])
    plt.tight_layout(pad=0.1,h_pad=0.1,w_pad=0.1)


# ---------------------------------------------------------------
def fname_gen(f, member, ID, year, month, day, hour, minute, second):

    # get field
    if f == 0:
        fieldname = "tanwind"
    elif f == 1:
        fieldname = "radwind"
    elif f == 2:
        fieldname = "verwind"
    elif f == 3:
        fieldname = "pres_msl"
    elif f == 4:
        fieldname = "lhfl_s"
    elif f == 5:
        fieldname = "vor"
    elif f == 6:
        fieldname = "temp"

    # add date
    fname = (
        member
        + ID
        + fieldname
        + year
        + month
        + day
        + "T"
        + hour
        + minute
        + second
        + "Z_redo.png"
    )

    # add out directory
    fname = "../out/" + fname

    return fname, fieldname
