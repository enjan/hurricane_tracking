# TC Tracking Algorithm

##TLDR
```bash
module load conda/2020
conda activate iacpy3_2020
python tctrack.py n2o_config.py
```

## n2o_config.py
This file contains the configuration that before was in the beginning of each script.
It now has to be passed as a command line argument to the scripts.

## do_one_member_jobs.py
this can be run from src like:
```bash
./do_one_member_jobs.py
```

This calls all scripts that are applied to only one member in a row:
```bash
#!/bin/bash
source /usr/local/Miniconda3/etc/profile.d/conda.sh
conda activate iacpy3_2020
echo "Loaded environments"
echo "Starting tctrack"
python tctrack.py n2o_config.py
echo "Starting tcstitch"
python tcstitch.py n2o_config.py
echo "starting tccat"
python tccat.py n2o_config.py
echo "starting plottrack"
python plottrack.py n2o_config.py
echo "Finished all one member jobs"
```
## plottrack_exp.py and plottrack_bycat.py
These scripts can work with several members. They use the variable "plot_members" in the "sim_params" dict from the config file n2o_config.py.
See here:
```python
sim_params = dict(
# Other parameters
    plot_members=["01"],  # ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10"]
)

```

They can be called like all scripts:
```
module load conda/2020
conda activate iacpy3_2020
python plottrack_exp.py n2o_config.py

```