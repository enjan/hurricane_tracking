#!/usr/bin/env python


def dataread(year, month, day, hour, minute, second, indir, inbase, fields, fieldsl):

    from netCDF4 import Dataset
    import numpy as np

    data = 1
    datal = 1

    # generate filename
    fname = indir + inbase + year + month + day + "T" + hour + minute + second + "Z.nc"
    print("reading " + fname)

    # read number of cells
    dataset = Dataset(fname)

    # allocate data
    ncells = dataset.dimensions["ncells"].size
    numlev = dataset.dimensions["height"].size
    data = np.zeros((len(fields), numlev, ncells))
    datal = np.zeros((len(fields), numlev + 1, ncells))

    # read requested fields
    for f in range(0, len(fields)):
        ndim = dataset.variables[fields[f]].ndim
        if ndim == 3:
            data[f, :, :] = dataset.variables[fields[f]][:]
            continue
        if ndim == 2:
            if fields[f] == "z_mc":
                data[f, :, :] = dataset.variables[fields[f]][:]
            else:
                data[f, 0, :] = dataset.variables[fields[f]][:]
            continue

    # read requested fields
    for f in range(0, len(fieldsl)):
        ndim = dataset.variables[fieldsl[f]].ndim
        if ndim == 3:
            datal[f, :, :] = dataset.variables[fieldsl[f]][:]
            continue
        if ndim == 2:
            if fieldsl[f] == "z_ifc":
                datal[f, :, :] = dataset.variables[fieldsl[f]][:]
            else:
                datal[f, 0, :] = dataset.variables[fieldsl[f]][:]
            continue

    # i print(hgt[:,222345])
    # read clon and clat
    clon = dataset.variables["clon"][:]
    clat = dataset.variables["clat"][:]

    return data, datal, clon, clat


# ---------------------------------------------------------------


def dataread_remap(year, month, day, hour, minute, second, fields):

    return data, lon, lat
