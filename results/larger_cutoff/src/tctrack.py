#!/usr/bin/env python
from utils import str_to_datetime, print_current_step, parse_arguments, read_config

from searchdude import searchtc
from datadude import readfile
from datadude import writetc
from winddude import windmax

import datetime
import os

import time

if __name__ == "__main__":
    tic = time.time()

    config_file = parse_arguments()
    tc_params, sim_params = read_config(config_file)

    curr_datetime = str_to_datetime(sim_params["initime"])
    end_datetime = str_to_datetime(sim_params["endtime"])

    step = 0

    with open(os.path.join("../out", sim_params["outname"]), "w") as outfile:

        # loop over time
        while curr_datetime != end_datetime:

            print_current_step(curr_datetime, step)

            # read data
            tc_data = readfile(sim_params, curr_datetime)

            # search for TCs
            tclonin, tclatin = searchtc(tc_data, tc_params)

            # skip to next iteration if no TCs found
            if tclonin[0] >= 1:
                # determine maximum wind
                tc_data["maxwind"] = windmax(tclonin, tclatin, tc_data, tc_params)

                # write to text file
                writetc(
                    curr_datetime, tclonin, tclatin, tc_data, outfile,
                )

            # update step number
            step = step + 1
            # update time
            curr_datetime += datetime.timedelta(hours=6)

    toc = time.time()
    print(
        "The search took {} seconds which is equal to {} minutes".format(
            toc - tic, (toc - tic) / 60.0
        )
    )
