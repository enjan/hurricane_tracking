#!/usr/bin/env python

import netCDF4

from utils import datetime_to_str

import os


def readfile(sim_params, curr_datetime):

    # create file names
    fname = os.path.join(
        sim_params["indir"],
        sim_params["inbase"] + datetime_to_str(curr_datetime) + ".nc",
    )
    fname2 = os.path.join(
        sim_params["indir2"],
        sim_params["inbase2"] + datetime_to_str(curr_datetime) + ".nc",
    )

    print("attempting to read file " + fname)

    dataset = netCDF4.Dataset(fname)
    tc_data = dict(
        slp=dataset.variables["pres_msl"][0],
        vor=dataset.variables["vor"][0, 32],
        u=dataset.variables["u"][0],
        v=dataset.variables["v"][0],
        hgt=dataset.variables["z_mc"][:],
        lon=dataset.variables["lon"][:],
        lat=dataset.variables["lat"][:],
    )
    dataset.close()

    print("attempting to read file " + fname2)

    dataset2 = netCDF4.Dataset(fname2)
    tc_data["tem"] = dataset2.variables["temp"][0, 0, :, :]
    dataset2.close()

    return tc_data


def writetc(
    curr_datetime, tclonin, tclatin, tc_data, outfile,
):
    fname = curr_datetime.strftime("%Y-%m-%dT%H:%M:%SZ")

    maxwind = tc_data["maxwind"]

    for t in range(0, maxwind.shape[0]):
        outfile.write(fname)
        outfile.write("\t")
        outfile.write(str(tclonin[t + 1]))
        outfile.write("\t")
        outfile.write(str(tclatin[t + 1]))
        outfile.write("\t")
        outfile.write(str(tc_data["lon"][tclonin[t + 1]]))
        outfile.write("\t")
        outfile.write(str(tc_data["lat"][tclatin[t + 1]]))
        outfile.write("\t")
        outfile.write(str(tc_data["slp"][tclatin[t + 1], tclonin[t + 1]]))
        outfile.write("\t")
        outfile.write(str(maxwind[t]))
        outfile.write("\n")
