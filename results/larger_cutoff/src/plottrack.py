#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.basemap import Basemap
from utils import parse_arguments, read_config


config_file = parse_arguments()
_, sim_params = read_config(config_file)

# sharpen the mix
member = sim_params["member"]
exp = sim_params["exp"]

# to all kinds of trix
infile = "../out/IDTC_" + member + "_" + exp + ".txt"
endtimes = False

# open file
file = open(infile, "r")

# open figure with map
mp = Basemap(
    llcrnrlon=-110.0,
    llcrnrlat=0.0,
    urcrnrlon=20.0,
    urcrnrlat=50.0,
    projection="lcc",
    lat_1=20.0,
    lat_2=40.0,
    lon_0=-60.0,
    resolution="l",
    area_thresh=1000.0,
)
mp.drawcoastlines()
mp.drawparallels(np.arange(10, 70, 20), labels=[1, 1, 0, 0])
mp.drawmeridians(np.arange(-140, 20, 20), labels=[0, 0, 0, 1])
plt.title("Hurricane Tracks   Member " + member + "   Experiment " + exp)

# read first line
dat = file.readline()
dat = dat.split()
oldlon = float(dat[4])
oldlat = float(dat[5])
ID = int(dat[0])

# loop over all lines
n = -1
while not endtimes:

    n = n + 1

    # read line
    dat = file.readline()
    dat = dat.split()

    try:
        newlon = float(dat[4])
        newlat = float(dat[5])
    except:
        break

    # check if new ID
    if int(dat[0]) != ID:
        oldlon = float(dat[4])
        oldlat = float(dat[5])
        ID = int(dat[0])
        continue

    # plot line
    lon = [oldlon, newlon]
    lat = [oldlat, newlat]
    #  lon = [-100,-80,-70]
    #  lat = [15,20,25]
    x, y = mp(lon, lat)
    mp.plot(x, y, color="r")

    # pass lon and lat
    oldlon = newlon
    oldlat = newlat

plt.savefig("../plots/tracks_" + member + "_" + exp + ".png")
