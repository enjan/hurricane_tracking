#!/usr/bin/env python

import numpy as np


def windmax(tclonin, tclatin, tc_data, tc_params):

    print("### windmax")

    # allocate maxwind
    maxwind = np.zeros(int(tclonin[0]))

    hgt = tc_data["hgt"]

    maxhgt = tc_params["maxhgt"]
    winddis = tc_params["winddis"]

    # determine maximum height
    maxhgtin = np.argwhere(hgt[:, tclatin[1], tclonin[1]] < maxhgt)
    maxhgtin = np.min(maxhgtin)
    if (hgt[maxhgtin, tclatin[1], tclonin[1]] - maxhgt) < (
        maxhgt - hgt[maxhgtin - 1, tclatin[1], tclonin[1]]
    ):
        maxhgtin = maxhgtin - 1

    # look for max distance over which to search
    mpdeg = 2 * np.pi * 6372339 / 360
    nind = round(winddis / (mpdeg * 180 / hgt.shape[1]))
    print("nind: ", nind)

    # loop over found TCs
    for t in range(1, tclonin[0] + 1):
        # calculate wind magnitude
        utemp = tc_data["u"][
            0:maxhgtin,
            tclatin[t] - nind : tclatin[t] + nind + 1,
            tclonin[t] - nind : tclonin[t] + nind + 1,
        ]
        vtemp = tc_data["v"][
            0:maxhgtin,
            tclatin[t] - nind : tclatin[t] + nind + 1,
            tclonin[t] - nind : tclonin[t] + nind + 1,
        ]

        maxwind[t - 1] = np.ma.sqrt(np.ma.max(utemp ** 2 + vtemp ** 2))
    print("maximum wind speed: " + str(maxwind[t - 1]))

    return maxwind
