#!/usr/bin/env python

import numpy as np
import skimage.feature


def searchtc(tc_data, tc_params):
    print("searching for TCs")
    ni = tc_data["slp"].shape[1]
    nj = tc_data["slp"].shape[0]
    tcloc = np.ones((nj, ni))

    tcloc = slpmin(tcloc, tc_data, tc_params)
    print("sum slploc: " + str(np.sum(tcloc)))

    # kick out edge region where interpolation is happening
    lonx, laty = np.meshgrid(tc_data["lon"], tc_data["lat"])
    mask = ~((1 <= laty) & (laty <= 69) & (lonx <= -16) & (-119 <= lonx))
    tcloc[mask] = 0

    #  tcloc = latmax(tcloc,ni,nj,maxlat)
    #  print('sum latmax: '+str(np.sum(tcloc)))

    tcloc = vorval(tcloc, tc_data, tc_params)
    print("sum vorloc: " + str(np.sum(tcloc)))

    tclonin, tclatin = temmax(tcloc, tc_data, tc_params)
    print("sum temmax: " + str(np.sum(tcloc)))

    return tclonin, tclatin


# --------------------------------------------------------------


def slpmin(tcloc, tc_data, tc_params):

    print("### slpmin")

    # determine number of indices of search box side length
    mpdeg = 2 * np.pi * 6371229 / 360
    nind = round(tc_params["slpdis"] / mpdeg * (1 / tc_params["res"]))
    print("nind:  " + str(nind))

    slp = tc_data["slp"]

    cond1 = tc_data["hgt"].mask[49, :, :]
    cond2 = slp < 88000
    cond3 = slp > 101000

    cond4 = skimage.feature.peak_local_max(
        -(slp.filled(slp.max())), min_distance=nind, indices=False, exclude_border=False
    )

    tcloc[cond1 | cond2 | cond3 | ~cond4] = 0

    return tcloc


# --------------------------------------------------------------


def latmax(tcloc, ni, nj, latmax):

    print("### latmax")

    # find index of lat cutoffs

    inpd = nj / 180  # indices per degree
    latmaxin = int(inpd * latmax)  # distance of latmax to eq in deg
    northin = int(latmaxin + nj / 2)
    southin = int(nj / 2 - latmaxin)

    tcloc[0:southin, :] = 0
    tcloc[northin:nj, :] = 0

    return tcloc


# --------------------------------------------------------------


def vorval(tcloc, tc_data, tc_params):

    print("### vorval")

    cond1 = tcloc == 0
    cond2 = tc_data["vor"] < tc_params["vormin"]
    tcloc[~cond1 & cond2] = 0

    return tcloc


# --------------------------------------------------------------


def temmax(tcloc, tc_data, tc_params):

    print("### temmax")

    # check how many indices away to search
    mpdeg = 2 * np.pi * 6371229 / 360
    nind = round(tc_params["temdis"] / mpdeg * (1 / tc_params["res"]))
    print("nind:  " + str(nind))

    n = 0
    tclonin = [0]
    tclatin = [0]

    tem = tc_data["tem"]
    temdif = tc_params["temdif"]

    for i, j in np.transpose(np.where(tcloc != 0)):
        # TODO fragen nach Boundary conditions
        # atem = np.mean(tem[max(i - nind,0) : min(i + nind,tem.shape[0]-1), j - nind : j + nind]) + temdif
        atem = np.mean(tem[i - nind : i + nind, j - nind : j + nind]) + temdif

        # compare to location
        if tem[i, j] < atem:
            tcloc[i, j] = 0
        else:
            print("lon: ", tc_data["lon"][j])
            print("lat: ", tc_data["lat"][i])
            n = n + 1
            tclonin.append(j)
            tclatin.append(i)

            # total number fo remaining tcs
    tclonin[0] = n
    tclatin[0] = n

    return np.array(tclonin), np.array(tclatin)
