#!/usr/bin/env python

import numpy as np

from utils import parse_arguments, read_config


config_file = parse_arguments()
_, sim_params = read_config(config_file)

# sharpen the mix
member = sim_params["member"]
exp = sim_params["exp"]

# to all kinds of trix
infname = "../out/IDTC_" + member + "_" + exp + ".txt"
outname = "../out/catIDTC_" + member + "_" + exp + ".txt"

U = np.zeros((1))
n = np.zeros((1))
t = 0
s = 0
ID = -1
therearelinestoread = True

cat = np.zeros((1))

# open file
infile = open(infname, "r")

while therearelinestoread:
    # read line
    dat = infile.readline()
    dat = dat.split()
    try:
        aa = dat[0]
    except:
        break

    # assign ID on first step
    if s == 0:
        ID = int(dat[0])

    # check if ID changed
    if int(dat[0]) != ID:
        ID = int(dat[0])
        t = t + 1
        U = np.append(U, [0])
        n = np.append(n, [0])

    # check wind speed
    U[t] = max(U[t], float(dat[7]))

    # update number of steps
    n[t] = n[t] + 1

    s = s + 1

# close file
infile.close()

# convert wind to category
cat = np.zeros((U.shape))
for i in range(0, U.shape[0]):
    if U[i] < 17:
        cat[i] = -1
    elif U[i] < 33:
        cat[i] = 0
    elif U[i] < 43:
        cat[i] = 1
    elif U[i] < 50:
        cat[i] = 2
    elif U[i] < 58:
        cat[i] = 3
    elif U[i] < 70:
        cat[i] = 4
    elif U[i] >= 70:
        cat[i] = 5

# create new file
outfile = open(outname, "w")

# go through old file and write line by line
infile = open(infname, "r")

s = 0
t = 0

while therearelinestoread:
    # read line
    dat = infile.readline()
    dat = dat.split()
    try:
        aa = dat[0]
    except:
        break

    # assign ID on first step
    if s == 0:
        ID = int(dat[0])

    # check if ID changed
    if int(dat[0]) != ID:
        ID = int(dat[0])
        t = t + 1
    #    U = np.append(U,[0])

    outfile.write(
        dat[0]
        + "\t"
        + str(cat[t])
        + "\t"
        + dat[1]
        + "\t"
        + dat[2]
        + "\t"
        + dat[3]
        + "\t"
        + dat[4]
        + "\t"
        + dat[5]
        + "\t"
        + dat[6]
        + "\t"
        + dat[7]
        + "\t"
        + str(n[t])
    )
    outfile.write("\n")

    s = s + 1

outfile.close()
