from ..utils import parse_arguments, read_config


if __name__ == "__main__":

    config_file = parse_arguments()
    tc_params, sim_params = read_config(config_file)

    print(tc_params, sim_params)
