#!/usr/bin/env python
import os

# sharpen the mix
member = "01"
exp = "ref"
sim_base_dir = "/wolke_scratch/enzb/ICON/output/sim_13k_largedomain"

sim_params = dict(
    initime="2013-06-01T000000Z",
    endtime="2013-12-01T000000Z",
    inbase="REMAP_NWP_LAM_DOM01_",
    inbase2="REMAP_NWP_LAM_PL_T_DOM01_",
    indir="{sim_base_dir}/2013_{member}_{exp}/remap/".format(
        sim_base_dir=sim_base_dir, member=member, exp=exp
    ),
    indir2="{sim_base_dir}/2013_{member}_{exp}/remap/".format(
        sim_base_dir=sim_base_dir, member=member, exp=exp
    ),
    member=member,
    exp=exp,
)
run_name = os.path.basename(__file__).replace(".py", "")
outdir = "../results/{run_name}".format(run_name=run_name)
out_params = dict(
    config_file=__file__,
    outdir=outdir,
    track_outname="{outdir}/out/TC_{member}_{exp}.csv".format(
        outdir=outdir, member=member, exp=exp
    ),
    stitch_outname="{outdir}/out/ID_TC_{member}_{exp}.csv".format(
        outdir=outdir, member=member, exp=exp
    ),
    plot_outdir="{outdir}/plots".format(outdir=outdir),
)

tc_params = dict(
    slpdis=50000,  # minimum distance between two slp minima
    maxlat=45,  # maximum latitude that is considered in degrees
    vormin=1e-5,  # minimum vorticity to pass as TC
    temdif=1,  # temperature difference of warm core
    temdis=200000,  # distance at which location is considered "environment"
    winddis=100000,  # distance within which to look for wind maximum
    maxhgt=3000,  # maximum height where mind maximum is searched
    res=0.125,  # resolution in degrees
)
