#!/usr/bin/env python
import os

sim_params = dict(
    initime="2013-06-01T000000Z",
    endtime="2013-12-01T000000Z",
    inbase="REMAP_NWP_LAM_DOM01_",
    inbase2="REMAP_NWP_LAM_PL_T_DOM01_",
)
run_name = os.path.basename(__file__).replace(".py", "")
outdir = "../results/{run_name}".format(run_name=run_name)
out_params = dict(
    config_file=__file__,
    outdir=outdir,
    plot_outdir="{outdir}/plots".format(outdir=outdir),
    csv_outdir="{outdir}/out".format(outdir=outdir),
)

tc_params = dict(
    slpdis=[50000],  # minimum distance between two slp minima
    maxlat=[45],  # maximum latitude that is considered in degrees
    vormin=[10e-6, 15e-6],  # minimum vorticity to pass as TC
    temdif=[0.5, 0.75, 1, 1.25, 1.5],  # temperature difference of warm core
    temdis=[200000, 100000],  # distance at which location is considered "environment"
    winddis=[100000],  # distance within which to look for wind maximum
    maxhgt=[3000],  # maximum height where mind maximum is searched
    res=[0.125],  # resolution in degrees
)
