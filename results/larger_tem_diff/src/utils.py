import datetime
import importlib
import argparse
import re
import os


def str_to_datetime(time_str):
    year = int(time_str[0:4])
    month = int(time_str[5:7])
    day = int(time_str[8:10])
    hour = int(time_str[11:13])
    minute = int(time_str[13:15])
    second = int(time_str[15:17])
    return datetime.datetime(year, month, day, hour, minute, second)


def datetime_to_str(datetime_obj):
    date = str(datetime_obj.date()).replace("-", "")
    time = str(datetime_obj.time()).replace(":", "")
    return date + "T" + time + "Z"


def print_current_step(curr_datetime, step):
    print()
    print("######################################\n")
    print("###", datetime_to_str(curr_datetime), "  step:", str(step).ljust(3), "###")
    print("######################################")


def is_python(arg_value, pat=re.compile(r"^.*\.(py)$")):
    if not pat.match(arg_value):
        raise argparse.ArgumentTypeError(
            "Invalid config file type. You need to provide a python file!"
        )
    if not os.path.isfile(arg_value):
        raise FileNotFoundError("Cannot find file: {}".format(arg_value))
    return arg_value


def parse_arguments():
    parser = argparse.ArgumentParser(
        description="Find and track tropical cyclones in ICON simulation data"
    )
    parser.add_argument(
        "config_file",
        type=is_python,
        help="Path to the required python config file in the same directory.",
    )
    args = parser.parse_args()
    return args.config_file.replace(".py", "")


def read_config(config_file):
    config = importlib.import_module(config_file)
    return config.tc_params, config.sim_params
