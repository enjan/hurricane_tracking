#!/usr/bin/env python

from timeupdate import timeupdate
from datareader import dataread
from meandude import meancalc
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.tri as tri
from mpl_toolkits.basemap import Basemap
import sys

# sharpen the mix
initime = "2013-08-01T000000Z"
endtime = "2013-11-01T000000Z"
inc = 3600 * 6

members = [
    ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10"],
    ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10"],
]
exp = ["ref", "rm"]

minheight = 2500
maxheight = 12500

hdir = (
    "/wolke_scratch/enzb/ICON/output/sim_13k_largedomain/"
    + initime[0:4]
    + "_"
    + members[0][0]
    + "_"
    + exp[0]
    + "/"
)
hbase = "NWP_LAM_DOM01_"
hind = 223456

indirb = "/wolke_scratch/enzb/ICON/output/sim_13k_largedomain/"
inbase = "NWP_LAM_DOM01_"

# to all kinds of trix
year = initime[0:4]
month = initime[5:7]
day = initime[8:10]
hour = initime[11:13]
minute = initime[13:15]
second = initime[15:17]

s = 0
dowhile = True

# determine indices for averaging
data, clon, clat = dataread(
    year, month, day, hour, minute, second, hdir, hbase, ["z_mc"]
)
height = data[0, :, hind]
booleheight = (height > minheight) * (height < maxheight)
nind = clat.size

# loop over time
while dowhile:

    # update time
    if s > 0:
        year, month, day, hour, minute, second = timeupdate(
            year, month, day, hour, minute, second, inc
        )
    if (
        year == endtime[0:4]
        and month == endtime[5:7]
        and day == endtime[8:10]
        and hour == endtime[11:13]
        and minute == endtime[13:15]
        and second == endtime[15:17]
    ):
        dowhile = False

    print("######################################")
    if s < 10:
        print(
            "### "
            + year
            + "-"
            + month
            + "-"
            + day
            + "T"
            + hour
            + minute
            + second
            + "Z"
            + "   step: "
            + str(s)
            + "   ###"
        )
    elif s < 100:
        print(
            "### "
            + year
            + "-"
            + month
            + "-"
            + day
            + "T"
            + hour
            + minute
            + second
            + "Z"
            + "   step: "
            + str(s)
            + "  ###"
        )
    else:
        print(
            "### "
            + year
            + "-"
            + month
            + "-"
            + day
            + "T"
            + hour
            + minute
            + second
            + "Z"
            + "   step: "
            + str(s)
            + " ###"
        )
    print("######################################")

    expmean = np.zeros((nind, len(exp)))
    e = 0

    # loop over exp
    for ex in exp:

        member = members[e]
        m = 0

        # loop over members
        for mem in member:

            indir = indirb + year + "_" + mem + "_" + ex + "/"

            # read qv data
            data, clon, clat = dataread(
                year, month, day, hour, minute, second, indir, inbase, ["qv"]
            )

            # calculate mean over height
            qv = booleheight[:, None] * data[0, :, :]
            qv = np.sum(qv, axis=0)

            # plot qv sum
            fig = plt.figure(figsize=(12, 8))
            mp = Basemap(
                llcrnrlon=-110.0,
                llcrnrlat=0.0,
                urcrnrlon=15.0,
                urcrnrlat=60.0,
                projection="lcc",
                lat_1=20.0,
                lat_2=40.0,
                lon_0=-60.0,
                resolution="l",
                area_thresh=1000.0,
            )
            mp.drawcoastlines()
            mp.drawparallels(np.arange(10, 70, 20), labels=[1, 1, 0, 0])
            mp.drawmeridians(np.arange(-140, 20, 20), labels=[0, 0, 0, 1])

            if e == 0 and m == 0:
                clon = clon * 180 / np.pi
                clat = clat * 180 / np.pi
                lon, lat = mp(clon, clat)

            mp.contourf(
                lon,
                lat,
                qv,
                cmap="Blues",
                tri=True,
                levels=[0, 0.02, 0.04, 0.06, 0.08, 0.1, 0.12, 0.14, 0.16],
            )
            cbar = mp.colorbar(location="bottom", pad=0.5)
            cbar.ax.set_xlabel(r"Specific Humidity [kg kg$^{-1}$]")
            plt.title(
                "Total Specific Humidity Between "
                + str(minheight)
                + " m and "
                + str(maxheight)
                + " m   exp="
                + ex
                + " member="
                + mem
                + " time="
                + year
                + "-"
                + month
                + "-"
                + day
                + "T"
                + hour
                + ":"
                + minute
                + ":"
                + second
                + "Z"
            )
            plt.savefig(
                "../out/qv/qv_contour_"
                + ex
                + "_"
                + mem
                + "_"
                + year
                + month
                + day
                + "T"
                + hour
                + minute
                + second
                + "Z.png"
            )
            plt.close()

            # add to exp mean
            expmean[:, e] = expmean[:, e] + qv

            # update member number
            m = m + 1

        # plot ensmean
        print("m = ", m)
        expmean[:, e] = expmean[:, e] / m
        fig = plt.figure(figsize=(12, 8))
        mp = Basemap(
            llcrnrlon=-110.0,
            llcrnrlat=0.0,
            urcrnrlon=15.0,
            urcrnrlat=60.0,
            projection="lcc",
            lat_1=20.0,
            lat_2=40.0,
            lon_0=-60.0,
            resolution="l",
            area_thresh=1000.0,
        )
        mp.drawcoastlines()
        mp.drawparallels(np.arange(10, 70, 20), labels=[1, 1, 0, 0])
        mp.drawmeridians(np.arange(-140, 20, 20), labels=[0, 0, 0, 1])

        mp.contourf(
            lon,
            lat,
            expmean[:, e],
            cmap="Blues",
            tri=True,
            levels=[0, 0.02, 0.04, 0.06, 0.08, 0.1, 0.12, 0.14, 0.16],
        )
        cbar = mp.colorbar(location="bottom", pad=0.5)
        cbar.ax.set_xlabel(r"Specific Humidity [kg kg$^{-1}$]")
        plt.title(
            "Total Specific Humidity Between "
            + str(minheight)
            + " m and "
            + str(maxheight)
            + " m   exp="
            + ex
            + "   ensemble mean   time="
            + year
            + "-"
            + month
            + "-"
            + day
            + "T"
            + hour
            + ":"
            + minute
            + ":"
            + second
            + "Z"
        )
        plt.savefig(
            "../out/qv/qv_contour_ensmean_"
            + ex
            + "_"
            + year
            + month
            + day
            + "T"
            + hour
            + minute
            + second
            + "Z.png"
        )
        plt.close()

        # update exp number
        e = e + 1

    # plot difference
    fig = plt.figure(figsize=(12, 8))
    mp = Basemap(
        llcrnrlon=-110.0,
        llcrnrlat=0.0,
        urcrnrlon=15.0,
        urcrnrlat=60.0,
        projection="lcc",
        lat_1=20.0,
        lat_2=40.0,
        lon_0=-60.0,
        resolution="l",
        area_thresh=1000.0,
    )
    mp.drawcoastlines()
    mp.drawparallels(np.arange(10, 70, 20), labels=[1, 1, 0, 0])
    mp.drawmeridians(np.arange(-140, 20, 20), labels=[0, 0, 0, 1])

    mp.contourf(
        lon,
        lat,
        expmean[:, 1] - expmean[:, 0],
        cmap="seismic",
        tri=True,
        levels=[-0.1, -0.08, -0.06, -0.04, -0.02, 0, 0.02, 0.04, 0.06, 0.08, 0.1],
    )
    cbar = mp.colorbar(location="bottom", pad=0.5)
    cbar.ax.set_xlabel(r"Specific Humidity [kg kg$^{-1}$]")
    plt.title(
        "Total Specific Humidity Between "
        + str(minheight)
        + " m and "
        + str(maxheight)
        + " m   exp="
        + exp[1]
        + " - "
        + exp[0]
        + "   time="
        + year
        + "-"
        + month
        + "-"
        + day
        + "T"
        + hour
        + ":"
        + minute
        + ":"
        + second
        + "Z"
    )
    plt.savefig(
        "../out/qv/qv_contour_ensdiff_"
        + year
        + month
        + day
        + "T"
        + hour
        + minute
        + second
        + "Z.png"
    )
    plt.close()

    # update step number
    s = s + 1
