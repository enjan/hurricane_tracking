#!/usr/bin/env python

# sharpen the mix
member = "01"
exp = "ref"
sim_base_dir = "/wolke_scratch/enzb/ICON/output/sim_13k_largedomain"

sim_params = dict(
    initime="2013-06-01T000000Z",
    endtime="2013-12-01T000000Z",
    member=member,
    exp=exp,
    indir="{sim_base_dir}/2013_{member}_{exp}/remap/".format(
        sim_base_dir=sim_base_dir, member=member, exp=exp
    ),
    indir2="{sim_base_dir}/2013_{member}_{exp}/remap/".format(
        sim_base_dir=sim_base_dir, member=member, exp=exp
    ),
    inbase="REMAP_NWP_LAM_DOM01_",
    inbase2="REMAP_NWP_LAM_PL_T_DOM01_",
    outname="TC_{member}_{exp}.txt".format(member=member, exp=exp),
    plot_members=[
        "01",
        "02",
        "03",
    ],  # ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10"]
)


tc_params = dict(
    slpdis=50000,  # minimum distance between two slp minima
    maxlat=45,  # maximum latitude that is considered in degrees
    vormin=1e-5,  # minimum vorticity to pass as TC
    temdif=3,  # temperature difference of warm core
    temdis=200000,  # distance at which location is considered "environment"
    winddis=100000,  # distance within which to look for wind maximum
    maxhgt=3000,  # maximum height where mind maximum is searched
    res=0.125,  # resolution in degrees
)
