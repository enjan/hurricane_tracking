#!/bin/bash
source /usr/local/Miniconda3/etc/profile.d/conda.sh
conda activate iacpy3_2019
echo "Loaded environments"
echo "Starting tctrack"
#python tctrack.py n2o_config.py
echo "Starting tcstitch"
python tcstitch.py n2o_config.py
echo "starting tccat"
python tccat.py n2o_config.py
echo "starting plottrack"
python plottrack.py n2o_config.py
echo "Finished all one member jobs"
