# Tropical Cyclone Tracking with varying parameter thresholds
This is the code for the semester project on Tropical Cyclone Tracking with varying parameter thresholds.
The Report can be found in the `handin` folder. 

## Repository Structure
```
hurricane_tracking
├── README.md
├── archive                         # original code
├── azimean                         # code for creating azimuthal mean plots for different charactersitic TC features
├── experiments                     # experimental code that was created in the development process
├── handin                          # handin material including report and supplementary plots
├── python                          # file that can be used to call scripts with the correct conda environment
├── report_hurricane_tracking       # git submodule of the github repo of the report. Probably only Jan has access
├── results                         # old results
└── src                             # ACTUAL SOURCE CODE!!!
```
## Source Code Structure 
```
src
├── 31_bernhard_test.py             # exemplary config file only running for very short time and few members
├── analysis_parallel.py            # actual script file performing the paralell analysis
├── data_analysis                   # directory containing jupyter notebooks where I performed data analysis steps
├── plotting.py                     # file for creating all track plots
├── run_configs                     # directory with exemplary run configs
├── searchdude.py                   # code performing the algortihm steps
├── stitching.py                    # code stitching together the different times teps
├── tctrack.py                      # looping through all different timesteps for tracking and calling searchdude
└── utils.py                        # utility functions

```

## Running the algorithm 

### ⚠️     This creates a directory `../results/31_bernhard_test/`.️ If the directory already exists this fails. ⚠️


### Classic
```bash
cd hurricane_tracking/src/
module load conda
conda activate iacpy3_2020
python parallel_analysis.py 31_bernhard_test.py
```

### In the background using Tmux
```bash
cd hurricane_tracking/src/
tmux new -s <your_job_name>
module load conda
conda activate iacpy3_2020
python parallel_analysis.py 31_bernhard_test.py
```



## Using Jupyter Notebook remotely
### Start Jupyter session on N2O

```bash
tmux new-session -s 'background jobs'
cd ~
module load conda
source activate iacpy3_2020
jupyter notebook --no-browser --port 55000
```

### On Mac to Forward jupyter
```bash
ssh -f -N -L localhost:8888:localhost:55000 n2o
```

