import pandas as pd
from datetime import timedelta
import numpy as np
from utils import calc_distance


def get_category(velocity):
    if velocity < 17:
        return -1
    elif velocity < 33:
        return 0
    elif velocity < 43:
        return 1
    elif velocity < 50:
        return 2
    elif velocity < 58:
        return 3
    elif velocity < 70:
        return 4
    elif velocity >= 70:
        return 5


class Tc:
    def __init__(self, tc_id, df_row):
        self.id = tc_id
        self.rows = [df_row]

    @property
    def lifetime(self):
        return len(self.rows)

    @property
    def lon(self):
        return self.rows[-1].lon

    @property
    def lat(self):
        return self.rows[-1].lat

    @property
    def last_datetime(self):
        return self.rows[-1].date

    @property
    def slp(self):
        return self.rows[-1].slp


class TcCollection:
    def __init__(self, df: pd.DataFrame, min_lifetime, timestep, max_speed):
        self.df = df.sort_values(by=["date"])
        self.max_dist = max_speed * timestep.seconds
        self.curr_tc_id = 1

        self.active_tcs = {}
        self.past_tcs = {}

        self.timestep = timestep
        self.min_lifetime = min_lifetime

        self.curr_datetime = df.date.min()
        self.prev_datetime = df.date.min()

    def delete_shortlived_active_tcs(self):
        for tc_id, tc in self.active_tcs.copy().items():
            if tc.lifetime < self.min_lifetime:
                del self.active_tcs[tc_id]

    def archive_all_tcs(self):
        self.process_interrupted_tcs(True)

    def process_interrupted_tcs(self, is_prev_timestep_without_tc):
        if is_prev_timestep_without_tc:
            self.delete_shortlived_active_tcs()
            self.past_tcs.update(self.active_tcs)
            self.active_tcs = {}
        else:
            for tc_id, tc in self.active_tcs.copy().items():
                is_tc_not_present_in_prev_timestep = (
                    self.curr_datetime - tc.last_datetime > self.timestep
                )
                if is_tc_not_present_in_prev_timestep:
                    if tc.lifetime > self.min_lifetime:
                        self.past_tcs[tc_id] = self.active_tcs.pop(tc_id)
                    else:
                        del self.active_tcs[tc_id]

    def update_time(self, row):
        self.prev_datetime = self.curr_datetime
        self.curr_datetime = row["date"]

    def create_new_tc(self, row):
        self.active_tcs[self.curr_tc_id] = Tc(self.curr_tc_id, row)
        self.curr_tc_id += 1

    def find_close_tcs(self, row):
        closest_tc_id = None
        closest_dist = float("inf")
        for tc_id, tc in self.active_tcs.copy().items():
            dist = calc_distance(row, tc)
            if not np.isnan(dist) and dist <= self.max_dist and dist < closest_dist:
                closest_tc_id = tc_id
                closest_dist = dist
        return self.active_tcs.get(closest_tc_id, None)

    def _stitch_tcs(self):
        for idx, row in self.df.iterrows():
            self.update_time(row)

            is_prev_timestep_without_tc = (
                self.curr_datetime - self.prev_datetime > self.timestep
            )
            # processes all tcs that were not there the last timestep or
            # archive all tcs if during one timestep no tc was found
            self.process_interrupted_tcs(is_prev_timestep_without_tc)

            if is_prev_timestep_without_tc:
                self.create_new_tc(row)
            else:
                closest_tc = self.find_close_tcs(row)

                # if location is not within a previous TC, create new ID
                if closest_tc is None:
                    self.create_new_tc(row)

                else:
                    # problem that at the same timestep there can be several entries for the same tc
                    # possibly should already be dealt with during tracking period
                    # however does need temporal information between timesteps
                    if closest_tc.last_datetime == self.curr_datetime:
                        # update if pressure minimum is more prominent, overwrite
                        if closest_tc.slp < row.slp:
                            closest_tc.rows[-1] = row
                    else:
                        closest_tc.rows.append(row)

        self.archive_all_tcs()

        out_df = self.prepare_output_dataframe()
        if out_df.empty:
            return None
        else:
            out_df = self.calculate_categories(out_df)
            return out_df

    def prepare_output_dataframe(self):
        flat = []
        for tc_id, tc in self.past_tcs.items():
            for row in tc.rows:
                row["tc_id"] = tc_id
                flat.append(row)
        return pd.DataFrame(flat)

    def calculate_categories(self, out_df):
        out_df["curr_cat"] = out_df.apply(lambda x: get_category(x["maxwind"]), axis=1)
        out_df["cat"] = out_df["curr_cat"].groupby(out_df["tc_id"]).transform("max")
        return out_df


def stitch_tcs(
    df: pd.DataFrame, min_lifetime=3, timestep=timedelta(hours=6), max_speed=20
):
    all_tcs = TcCollection(df, min_lifetime, timestep, max_speed)
    stitched_df = all_tcs._stitch_tcs()
    return stitched_df


if __name__ == "__main__":
    import pandas as pd
    from utils import load_dataframe

    pwd = "/home/enjan/hurricane_tracking"
    df = load_dataframe(f"{pwd}/results/20_big_run/merged_results.csv")
    df1 = load_dataframe(f"{pwd}/results/20_big_run/stitch_tc_01_ref.csv")
    df2 = load_dataframe(f"{pwd}/results/20_big_run/stitch_tc_02_ref.csv")
    df3 = load_dataframe(f"{pwd}/results/20_big_run/stitch_tc_03_ref.csv")

    results = [df1, df2, df3]

    for result, (mem, exp) in zip(
        results, [("01", "ref"), ("02", "ref"), ("03", "ref")]
    ):
        result["mem"] = mem
        result["exp"] = exp

    # final = merge_and_process_results(results)

    print("TEST")

    # output["test_tc_id"] = output.groupby(
    #     [output.tc_id, output.mem, output.exp, output.param_id]
    # ).ngroup()
    #
    #
    # output.groupby(("tc_id")).apply(len).min()
