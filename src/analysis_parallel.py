import datetime
import json
import multiprocessing as mp

import netCDF4
import numpy as np
import pandas as pd
from sklearn.model_selection import ParameterGrid

import plotting
from stitching import stitch_tcs
from tctrack import find_tcs
from utils import read_config, save_dateframe, prepare_output_directory, timing


def get_threshold_parameter_grid(tc_params, sim_params):
    json_param_file = sim_params.get("json_param_file", False)
    if json_param_file:
        print("Loaded config from json")
        with open(json_param_file) as fin:
            parameter_list = json.load(fin)
    else:
        parameter_list = ParameterGrid(tc_params)
        parameter_list = [
            {**params, **dict(param_id=idx)}
            for idx, params in enumerate(parameter_list)
        ]
    return parameter_list


def prepare_parameter_list(
    tc_params, sim_params, members, experiments, sim_base_dir, outdir
):
    parameter_list = []
    combinations = []
    threshold_parameter_list = get_threshold_parameter_grid(tc_params, sim_params)
    for mem in members:
        for exp in experiments:
            run_params = sim_params.copy()
            indir = f"{sim_base_dir}/2013_{mem}_{exp}/remap/"
            run_params["indir"] = indir
            run_params["indir2"] = indir
            run_params["member"] = mem
            run_params["exp"] = exp
            parameter_list.append((threshold_parameter_list, run_params, outdir))
            combinations.append((mem, exp))

    return parameter_list, combinations, pd.DataFrame(threshold_parameter_list)


def find_and_stitch(parameters):
    parameter_list, run_params, outdir = parameters
    found_tcs = find_tcs(run_params, parameter_list)
    file = f"{outdir}/find_tc_{run_params['member']}_{run_params['exp']}.csv"
    save_dateframe(found_tcs, file)

    grouped = found_tcs.groupby([found_tcs.param_id])
    grouped_results = []
    for name, group in grouped:
        res = stitch_tcs(group)
        if res is not None:
            grouped_results.append(res)

    stitched_df = pd.concat(grouped_results, ignore_index=True)

    file = f"{outdir}/stitch_tc_{run_params['member']}_{run_params['exp']}.csv"
    save_dateframe(stitched_df, file)
    return stitched_df


def assign_unique_tc_id_and_sort_df(stitched_df):
    stitched_df.sort_values(["date", "param_id", "tc_id", "mem", "exp"], inplace=True)
    stitched_df.reset_index(drop=True, inplace=True)
    stitched_df["tc_id"] = stitched_df.groupby(
        ["tc_id", "param_id", "mem", "exp"]
    ).ngroup()
    stitched_df.sort_values(["tc_id", "date"], inplace=True)
    stitched_df.reset_index(drop=True, inplace=True)


def interpolate_sst(row, sst):
    month_idx = row.date.month - 4
    if row.date.month == 12:
        result = sst[month_idx][row.lat_idx][row.lon_idx]
    else:
        date_after = datetime.datetime(year=2013, month=row.date.month + 1, day=1)
        date_before = datetime.datetime(year=2013, month=row.date.month, day=1)
        month_delta = date_after - date_before
        ratio_before = (row.date - date_before) / month_delta
        ratio_after = (date_after - row.date) / month_delta
        month_idx = row.date.month - 4
        result = (
            ratio_before * sst[month_idx][row.lat_idx][row.lon_idx]
            + ratio_after * sst[month_idx + 1][row.lat_idx][row.lon_idx]
        )
    if np.ma.is_masked(result):
        return np.nan
    return result


def write_sst(stitched_df):
    def assign_genesis_sst(group, sst):
        group["genesis_sst"] = interpolate_sst(group.iloc[0], sst)
        return group

    dataset = netCDF4.MFDataset(
        "/wolke_scratch/enzb/ICON/output/sim_13k_largedomain/SST/2013/REMAP_SST*.nc"
    )
    sst = dataset.variables["SST"][:]
    dataset.close()
    return stitched_df.groupby(["tc_id"]).apply(lambda x: assign_genesis_sst(x, sst))


def merge_and_process_results(results, combinations):
    for result, (mem, exp) in zip(results, combinations):
        result["mem"] = mem
        result["exp"] = exp

    stitched_df = pd.concat(results, ignore_index=True)

    assign_unique_tc_id_and_sort_df(stitched_df)

    stitched_df = write_sst(stitched_df)

    return stitched_df


@timing
def main():
    (
        tc_params,
        sim_params,
        out_params,
        members,
        experiments,
        sim_base_dir,
    ) = read_config()
    outdir = prepare_output_directory(out_params)

    parameter_list, combinations, threshold_parameter_list = prepare_parameter_list(
        tc_params, sim_params, members, experiments, sim_base_dir, outdir
    )
    threshold_parameter_list.to_hdf(f"{outdir}/algo_output.hdf5", "pdf", format="table")
    pool = mp.Pool(processes=len(members) * len(experiments))
    results = pool.map(find_and_stitch, parameter_list)

    output = merge_and_process_results(results, combinations)

    output.to_hdf(f"{outdir}/algo_output.hdf5", "df", format="table")

    plotting.plot_curr_intensity_coloring(
        output, f'{out_params["plot_outdir"]}/curr_intensity_plot.png', min_lifetime=5
    )
    plotting.plot_max_intensity_coloring(
        output, f'{out_params["plot_outdir"]}/max_intensity_plot.png', min_lifetime=5
    )

    # filter out tcs that have less than 24C sst at genesis
    plotting.plot_curr_intensity_coloring(
        output[output.genesis_sst > 297.15],
        f'{out_params["plot_outdir"]}/curr_intensity_plot_sst.png',
        min_lifetime=5,
    )
    plotting.plot_max_intensity_coloring(
        output[output.genesis_sst > 297.15],
        f'{out_params["plot_outdir"]}/max_intensity_plot_sst.png',
        min_lifetime=5,
    )


if __name__ == "__main__":
    main()
