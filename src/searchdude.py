#!/usr/bin/env python

import numpy as np
import skimage.feature


def searchtc(tc_data, tc_params):
    ni = tc_data["slp"].shape[1]
    nj = tc_data["slp"].shape[0]
    tcloc = np.ones((nj, ni))

    tcloc = slpmin(tcloc, tc_data, tc_params)

    # kick out edge region where interpolation is happening
    lonx, laty = np.meshgrid(tc_data["lon"], tc_data["lat"])
    mask = ~((1 <= laty) & (laty <= 69) & (lonx <= -16) & (-119 <= lonx))
    tcloc[mask] = 0

    #  tcloc = latmax(tcloc,ni,nj,maxlat)
    #  print('sum latmax: '+str(np.sum(tcloc)))

    tcloc = vorval(tcloc, tc_data, tc_params)

    tclonin, tclatin = temmax(tcloc, tc_data, tc_params)

    return tclonin, tclatin


# --------------------------------------------------------------


def slpmin(tcloc, tc_data, tc_params):

    # determine number of indices of search box side length
    mpdeg = 2 * np.pi * 6371229 / 360
    nind = round(tc_params["slpdis"] / mpdeg * (1 / tc_params["res"]))

    slp = tc_data["slp"]

    cond1 = tc_data["hgt"].mask[49, :, :]
    cond2 = slp < 88000
    cond3 = slp > 101000

    cond4 = skimage.feature.peak_local_max(
        -(slp.filled(slp.max())), min_distance=nind, indices=False, exclude_border=False
    )

    tcloc[cond1 | cond2 | cond3 | ~cond4] = 0

    return tcloc


# --------------------------------------------------------------


def latmax(tcloc, ni, nj, latmax):

    # find index of lat cutoffs

    inpd = nj / 180  # indices per degree
    latmaxin = int(inpd * latmax)  # distance of latmax to eq in deg
    northin = int(latmaxin + nj / 2)
    southin = int(nj / 2 - latmaxin)

    tcloc[0:southin, :] = 0
    tcloc[northin:nj, :] = 0

    return tcloc


# --------------------------------------------------------------


def vorval(tcloc, tc_data, tc_params):

    cond1 = tcloc == 0
    cond2 = tc_data["vor"] < tc_params["vormin"]
    tcloc[~cond1 & cond2] = 0

    return tcloc


# --------------------------------------------------------------


def temmax(tcloc, tc_data, tc_params):

    # check how many indices away to search
    mpdeg = 2 * np.pi * 6371229 / 360
    nind = round(tc_params["temdis"] / mpdeg * (1 / tc_params["res"]))

    n = 0
    tclonin = [0]
    tclatin = [0]

    tem = tc_data["tem"]
    temdif = tc_params["temdif"]

    for i, j in np.transpose(np.where(tcloc != 0)):
        top = max(i - nind, 0)
        bottom = min(i + nind, tem.shape[0])
        left = max(j - nind, 0)
        right = min(j + nind, tem.shape[1])
        area = tem[top:bottom, left:right]

        is_at_edge = (area == np.ma.masked).data.any()
        if is_at_edge:
            tcloc[i, j] = 0
        # compare to surrounding area
        elif tem[i, j] < area.mean() + temdif:
            tcloc[i, j] = 0
        else:
            n = n + 1
            tclonin.append(j)
            tclatin.append(i)

            # total number fo remaining tcs
    tclonin[0] = n
    tclatin[0] = n

    return np.array(tclonin), np.array(tclatin)


def windmax(tclonin, tclatin, tc_data, tc_params):

    # allocate maxwind
    maxwind = np.zeros(int(tclonin[0]))

    hgt = tc_data["hgt"]

    maxhgt = tc_params["maxhgt"]
    winddis = tc_params["winddis"]

    # determine maximum height index
    maxhgtin = np.min(np.argwhere(hgt[:, 270, 420] < maxhgt))

    # look for max distance over which to search
    mpdeg = 2 * np.pi * 6372339 / 360
    nind = round(winddis / (mpdeg * 180 / hgt.shape[1]))

    # loop over found TCs
    for t in range(1, tclonin[0] + 1):
        # calculate wind magnitude
        utemp = tc_data["u"][
            maxhgtin:,
            tclatin[t] - nind : tclatin[t] + nind + 1,
            tclonin[t] - nind : tclonin[t] + nind + 1,
        ]
        vtemp = tc_data["v"][
            maxhgtin:,
            tclatin[t] - nind : tclatin[t] + nind + 1,
            tclonin[t] - nind : tclonin[t] + nind + 1,
        ]

        maxwind[t - 1] = np.ma.sqrt(np.ma.max(utemp ** 2 + vtemp ** 2))

    return maxwind
