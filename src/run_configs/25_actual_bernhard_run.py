import os

members = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10"]
experiments = ["ref", "rm"]
sim_base_dir = "/wolke_scratch/enzb/ICON/output/sim_13k_largedomain"

sim_params = dict(
    initime="2013-06-01T000000Z",
    endtime="2013-12-01T000000Z",
    inbase="REMAP_NWP_LAM_DOM01_",
    inbase2="REMAP_NWP_LAM_PL_T_DOM01_",
    json_param_file="bernhard_parameters.json",
)

run_name = os.path.basename(__file__).replace(".py", "")
outdir = "../results/{run_name}".format(run_name=run_name)
out_params = dict(
    config_file=__file__,
    outdir=outdir,
    plot_outdir="{outdir}/plots".format(outdir=outdir),
    csv_outdir="{outdir}/out".format(outdir=outdir),
)

tc_params = dict(
    slpdis=[
        100000
    ],  # check if there is even a difference? 100km sinnvoll fujiwara(H) oyama modell anschauen effect 2 tcs minimum distance between two slp minima
    vormin=[
        1e-5,
        1e-6,
        1e-4,
    ],  # bei denen lassen minimum vorticity to pass as TC, hat vormin überhaupt einen einfluss?, eher nicht
    temdif=[0.75, 0.5, 1, 1.25, 1.5],  # temperature difference of warm core
    temdis=[
        100000,
        150000,
        200000,
        250000,
        300000,
        400000,
    ],  # erwartung dass wenn temdis klein und temdif gross  =>  weniger #400km zu viel, überprüfe # distance at which location is considered "environment"
    winddis=[
        100000
    ],  # distance within which to look for wind maximum, ist Beschreibung hat nix mit tracking zu tun, bei 100km lassen, 100 sehr sicher
    maxhgt=[
        3000,
        2500,
        2000,
        1500,
        1000,
    ],  # maximum height where wind maximum is searched
    res=[0.125],  # resolution in degrees
)


# Bernhard Advice
# # MAKE SURE THAT I IN THE END HAVE ALL NECESSARY COMBINATIONS
#
# #minmal
# tc_params = dict(
#     slpdis=[25000, 50000, 100000],  # check if there is even a difference? 100km sinnvoll fujiwara(H) oyama modell anschauen effect 2 tcs minimum distance between two slp minima
#     vormin=[1e-5, 1e-6, 1e-4],  # bei denen lassen minimum vorticity to pass as TC, hat vormin überhaupt einen einfluss?, eher nicht
#     temdif=[0.75, 0.5, 1, 1.25, 1.5],  # temperature difference of warm core
#     temdis=[100000, 150000, 200000, 250000, 300000, 400000],# erwartung dass wenn temdis klein und temdif gross  =>  weniger #400km zu viel, überprüfe # distance at which location is considered "environment"
#     winddis=[100000],  # distance within which to look for wind maximum, ist Beschreibung hat nix mit tracking zu tun, bei 100km lassen, 100 sehr sicher
#     maxhgt=[3000, 2500, 2000, 1500, 1000],  # maximum height where wind maximum is searched
#     res=[0.125],  # resolution in degrees
# )
# members = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10"]
# experiments = ["ref", "rm"]
#
# # Änderung bei temdis
# # Änderung bei maxhgt
