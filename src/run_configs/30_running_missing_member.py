#!/usr/bin/env python
import os

members = ["03"]
experiments = ["ref"]
sim_base_dir = "/wolke_scratch/enzb/ICON/output/sim_13k_largedomain"

sim_params = dict(
    initime="2013-06-01T060000Z",
    endtime="2013-12-01T000000Z",
    inbase="REMAP_NWP_LAM_DOM01_",
    inbase2="REMAP_NWP_LAM_PL_T_DOM01_",
)

run_name = os.path.basename(__file__).replace(".py", "")
outdir = "../results/{run_name}".format(run_name=run_name)
out_params = dict(
    config_file=__file__,
    outdir=outdir,
    plot_outdir="{outdir}/plots".format(outdir=outdir),
    csv_outdir="{outdir}/out".format(outdir=outdir),
)

tc_params = dict(
    slpdis=[50000, 100000, 150000, 200000],  # minimum distance between two slp minima
    vormin=[1e-6, 1e-5, 1e-4, 1e-3],  # minimum vorticity to pass as TC
    temdif=[0.5, 0.75, 1.0, 1.25, 1.5],  # temperature difference of warm core
    temdis=[
        50000,
        100000,
        200000,
        300000,
        400000,
    ],  # distance at which location is considered "environment"
    winddis=[100000],  # distance within which to look for wind maximum
    maxhgt=[3000],  # maximum height where wind maximum is searched
    res=[0.125],  # resolution in degrees
)
