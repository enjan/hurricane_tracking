import numpy as np
import dask.dataframe as dd
import multiprocessing as mp
from jans_azimean_version import create_tc_plots

np.random.seed(42)

algo_data_file = "/home/enjan/hurricane_tracking/results/analysis/algo_output.hdf5"
df = dd.read_hdf(algo_data_file, "df")

sdf = df[(df.genesis_sst > 297.15) & (df.cat >= 1) & (df.param_id == 173)].compute()
tc_ids = np.random.choice(sdf.tc_id.unique(), 10)
maxwind_row_idcs = sdf[sdf.tc_id.isin(tc_ids)].groupby("tc_id").maxwind.idxmax()
plot_df = sdf[sdf.index.isin(maxwind_row_idcs)]
plot_df.to_csv("10_chosen_tcs.csv")
# inputs = [(row, 32) for idx, row in plot_df.iterrows()]

# pool = mp.Pool(10)
# results = pool.starmap(create_tc_plots, inputs)
# print(inputs)
# print(results)
