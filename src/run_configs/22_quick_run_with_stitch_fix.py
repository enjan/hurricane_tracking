#!/usr/bin/env python
import os

members = ["01", "02"]  # , "03", "04", "05", "06", "07", "08", "09", "10"]
experiments = ["ref"]  # , "rm"]
sim_base_dir = "/wolke_scratch/enzb/ICON/output/sim_13k_largedomain"

sim_params = dict(
    initime="2013-06-01T000000Z",
    endtime="2013-07-01T000000Z",
    inbase="REMAP_NWP_LAM_DOM01_",
    inbase2="REMAP_NWP_LAM_PL_T_DOM01_",
)

run_name = os.path.basename(__file__).replace(".py", "")
outdir = "../results/{run_name}".format(run_name=run_name)
out_params = dict(
    config_file=__file__,
    outdir=outdir,
    plot_outdir="{outdir}/plots".format(outdir=outdir),
    csv_outdir="{outdir}/out".format(outdir=outdir),
)

tc_params = dict(
    slpdis=[25000],  # , 50000, 100000],  # minimum distance between two slp minima
    maxlat=[45],  # maximum latitude that is considered in degrees
    vormin=[1e-5],  # , 1e-6, 1e-4],  # minimum vorticity to pass as TC
    temdif=[0.75, 0.5],  # , 1, 1.25],  # temperature difference of warm core
    temdis=[200000],  # distance at which location is considered "environment"
    winddis=[
        50000
    ],  # , 100000, 200000],  # distance within which to look for wind maximum
    maxhgt=[3000],  # 2500],  # maximum height where wind maximum is searched
    res=[0.125],  # resolution in degrees
)
