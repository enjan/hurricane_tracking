import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.colors import to_rgba
from matplotlib import collections
import matplotlib.gridspec as gridspec
from mpl_toolkits.basemap import Basemap
from tqdm import tqdm
import seaborn as sns
from sklearn.neighbors import KernelDensity

import netCDF4
import utils

num_to_cat_mapping = {
    -1: "Tropical depression",
    0: "Tropical storm",
    1: "TC Category 1",
    2: "TC Category 2",
    3: "TC Category 3",
    4: "TC Category 4",
    5: "TC Category 5",
}

ss_scale = {
    -1: "#5ebaff",
    0: "#00faf4",
    1: "#ffffcc",
    2: "#ffe775",
    3: "#ffc140",
    4: "#ff8f20",
    5: "#ff6060",
}
simpson_ss_scale_rgba = {key: to_rgba(val) for key, val in ss_scale.items()}


ss_scale_rgba = {
    -1: simpson_ss_scale_rgba[-1],
    0: simpson_ss_scale_rgba[0],
    1: (1, 0.9, 0),
    2: (1, 0.7, 0),
    3: (1, 0.5, 0),
    4: (1, 0.3, 0),
    5: (1, 0.1, 0),
}

bmap_params = dict(
    llcrnrlon=-113.0,
    llcrnrlat=-20.0,
    urcrnrlon=40.0,
    urcrnrlat=60.0,
    projection="lcc",
    lat_1=20.0,
    lat_2=40.0,
    lon_0=-60.0,
    resolution="l",
    area_thresh=1000.0,
)


def get_category_legend(category_color_dict):
    from matplotlib.lines import Line2D

    custom_lines = [
        Line2D([0], [0], color=color, lw=4) for color in category_color_dict.values()
    ]
    labels = [num_to_cat_mapping[cat] for cat in category_color_dict]
    return custom_lines, labels


def plot_curr_intensity_coloring(
    df,
    filename=None,
    min_lifetime=3,
    bmap_params=bmap_params,
    title="Snapshot category coloring",
):
    data = df.groupby("tc_id").filter(lambda x: len(x) >= min_lifetime)

    fig = plt.figure(figsize=(12, 8), dpi=300)
    ax1 = fig.add_subplot(1, 1, 1)

    mp = Basemap(
        **bmap_params,
        ax=ax1,
    )
    mp.drawcoastlines()
    mp.drawparallels(np.arange(10, 70, 20), labels=[1, 1, 0, 0])
    mp.drawmeridians(np.arange(-140, 20, 20), labels=[0, 0, 0, 1])

    grouped = data.groupby("tc_id")
    for name, group in tqdm(grouped):
        lon, lat = mp(group["lon"].values, group["lat"].values)
        points = np.array([lon, lat]).T.reshape(-1, 1, 2)
        segments = np.concatenate([points[:-1], points[1:]], axis=1)
        mp.scatter(lon[0], lat[0], marker="x", color="k")
        colors = group["curr_cat"].apply(lambda x: ss_scale_rgba[x])
        lc = collections.LineCollection(segments, colors=colors)
        # mp.scatter(positions[0][0], positions[1][0], marker="x", color="k", linewidths=0.75)
        ax1.add_collection(lc)

    custom_lines, labels = get_category_legend(ss_scale_rgba)
    ax1.legend(custom_lines, labels, loc="lower left")

    if title:
        plt.title(title)
    if filename:
        fig.savefig(filename)
    return fig


def plot_max_intensity_coloring(
    df, filename, min_lifetime=3, title="Maximum category coloring"
):

    data = df.groupby("tc_id").filter(lambda x: len(x) >= min_lifetime)

    fig = plt.figure(figsize=(12, 8), dpi=300)
    ax1 = fig.add_subplot(1, 1, 1)

    mp = Basemap(
        **bmap_params,
        ax=ax1,
    )
    mp.drawcoastlines()
    mp.drawparallels(np.arange(10, 70, 20), labels=[1, 1, 0, 0])
    mp.drawmeridians(np.arange(-140, 20, 20), labels=[0, 0, 0, 1])

    lines = []
    colors = []
    grouped = data.sort_values(["date"]).groupby("tc_id")
    for name, group in grouped:
        positions = mp(group["lon"].values, group["lat"].values)
        lines.append(np.stack(positions).T)
        mp.scatter(
            positions[0][0], positions[1][0], marker="x", color="k", linewidths=0.75
        )
        colors.append(ss_scale_rgba[group["cat"].iloc[0]])

    lc = collections.LineCollection(lines, colors=colors)
    ax1.add_collection(lc)

    custom_lines, labels = get_category_legend(ss_scale_rgba)
    ax1.legend(custom_lines, labels, loc="lower left")

    if title:
        plt.title(title)
    fig.savefig(filename)


def plot_tc_genesis_with_density(genesis_df: pd.DataFrame, filename: str):
    tc_positions = genesis_df[["lat", "lon"]].values[:]
    kde = KernelDensity(
        bandwidth=0.15, metric="haversine", kernel="cosine", algorithm="ball_tree"
    )
    kde.fit(np.deg2rad(tc_positions))

    bmap_params = dict(
        llcrnrlon=-113.0,
        llcrnrlat=-9.0,
        urcrnrlon=10.0,
        urcrnrlat=35.0,
        projection="lcc",
        lat_1=20.0,
        lat_2=40.0,
        lon_0=-60.0,
        resolution="l",
        area_thresh=1000.0,
    )

    fig = plt.figure(figsize=(12, 8), dpi=300)
    ax1 = fig.add_subplot(1, 1, 1)

    mp = Basemap(
        **bmap_params,
        ax=ax1,
    )
    fname = "/wolke_scratch/enzb/ICON/output/sim_13k_largedomain/2013_01_ref/remap/REMAP_NWP_LAM_DOM01_20130601T000000Z.nc"
    dataset = netCDF4.Dataset(fname)
    grid_lon = np.deg2rad(dataset.variables["lon"][::3])
    grid_lat = np.deg2rad(dataset.variables["lat"][::3])
    dataset.close()
    xx, yy = np.meshgrid(grid_lon, grid_lat)
    xy = np.vstack([yy.ravel(), xx.ravel()]).T
    print("Score samples")
    Z = np.exp(kde.score_samples(xy))
    Z = Z.reshape(xx.shape)
    xx, yy = mp(np.rad2deg(xx), np.rad2deg(yy))
    pos = mp(tc_positions[:, 1], tc_positions[:, 0])

    mp.drawcoastlines()
    mp.drawparallels(np.arange(10, 70, 20), labels=[1, 1, 0, 0])
    mp.drawmeridians(np.arange(-140, 20, 20), labels=[0, 0, 0, 1])

    levels = np.linspace(0.05, Z.max(), 25)
    den = plt.contourf(xx, yy, Z, levels=levels, cmap=plt.cm.Reds)
    mp.scatter(pos[0].ravel(), pos[1].ravel(), marker=".", color="r")
    mp.colorbar(
        den,
        ax=ax1,
        location="bottom",
        pad=0.3,
        label=f"{kde.kernel.capitalize()} Kernel Density",
    )
    plt.title("TC Genesis")

    if filename:
        fig.savefig(filename)


def calc_normalized_occs(data):
    data["occs"] = data.groupby(["mem", "exp", "date", "lon_idx", "lat_idx"])[
        "tc_id"
    ].transform("size")
    data["noccs"] = data.groupby(["mem", "exp"])["occs"].transform(
        lambda x: x / x.max()
    )


def plot_occurrence(
    df,
    calc_occ=False,
    filename=None,
    min_lifetime=3,
    bmap_params=None,
    title=None,
):
    if bmap_params is None:
        bmap_params = dict(
            llcrnrlon=-100.0,
            llcrnrlat=-5.0,
            urcrnrlon=10.0,
            urcrnrlat=60.0,
            projection="lcc",
            lat_1=20.0,
            lat_2=40.0,
            lon_0=-60.0,
            resolution="l",
            area_thresh=1000.0,
        )
    if min_lifetime > 3:
        data = df.groupby("tc_id").filter(lambda x: len(x) >= min_lifetime).copy()
    else:
        data = df.copy()
    if calc_occ or not "noccs" in data:
        calc_normalized_occs(data)

    fig = plt.figure(figsize=(12, 8), dpi=300)
    ax1 = fig.add_subplot(1, 1, 1)

    mp = Basemap(
        **bmap_params,
        ax=ax1,
    )
    mp.drawcoastlines()
    mp.drawparallels(np.arange(10, 70, 20), labels=[1, 1, 0, 0])
    mp.drawmeridians(np.arange(-140, 20, 20), labels=[0, 0, 0, 1])

    grouped = data.groupby("tc_id")
    cmap = plt.cm.winter

    for name, group in tqdm(grouped):
        lon, lat = mp(group["lon"].values, group["lat"].values)
        points = np.array([lon, lat]).T.reshape(-1, 1, 2)
        segments = np.concatenate([points[:-1], points[1:]], axis=1)
        mp.scatter(lon[0], lat[0], marker="x", color="k")
        colors = cmap(group.noccs)
        lc = collections.LineCollection(segments, colors=colors)
        ax1.add_collection(lc)
    cbar = plt.colorbar(mappable=plt.cm.ScalarMappable(cmap=cmap))
    cbar.ax.set_ylabel(
        "point occurrence frequency in all parameter combinations",
        rotation=-90,
        va="bottom",
        fontsize="x-large",
    )
    if title:
        plt.title(title, fontsize="xx-large")
    if filename:
        fig.savefig(filename)
    return fig


def close_tracks(group, ref_tc):
    tmp_group = group[(group.date == ref_tc.date)]
    if len(tmp_group.index) == 0:
        return False
    tmp_tc = tmp_group.iloc[0]  # works because each tc only has one entry per timestep
    if utils.calc_distance(tmp_tc, ref_tc) < 70000:
        return True
    else:
        return False


def plot_matching_tracks_full_map(
    df,
    tc_id_list,
    filename=None,
    min_lifetime=3,
    bmap_params=bmap_params,
    title=None,
):
    if min_lifetime > 3:
        data = df.groupby("tc_id").filter(lambda x: len(x) >= min_lifetime)
    else:
        data = df

    data["lifetime"] = data.groupby("tc_id")["mem"].transform("size")

    data["occs"] = data.groupby(["date", "lon_idx", "lat_idx"])["tc_id"].transform(
        "size"
    )
    data["noccs"] = data["occs"] / data["occs"].max()

    fig = plt.figure(figsize=(12, 8), dpi=300)
    ax1 = fig.add_subplot(1, 1, 1)

    grouped = data.groupby("tc_id")
    print("#groups: ", len(grouped))

    mp = Basemap(
        **bmap_params,
        ax=ax1,
    )
    mp.drawcoastlines()
    mp.drawparallels(np.arange(10, 70, 20), labels=[1, 1, 0, 0])
    mp.drawmeridians(np.arange(-140, 20, 20), labels=[0, 0, 0, 1])

    for tc_id in tc_id_list:
        base_tc_ls = grouped.get_group(tc_id)
        base_tc = base_tc_ls.loc[base_tc_ls["maxwind"].idxmax()]

        blon, blat = mp(base_tc.lon, base_tc.lat)
        mp.scatter(blon, blat, marker="x", color="r")

        filtered = data.groupby("tc_id").filter(lambda x: close_tracks(x, base_tc))

        for name, group in tqdm(filtered.groupby("tc_id")):
            lon, lat = mp(group["lon"].values, group["lat"].values)
            points = np.array([lon, lat]).T.reshape(-1, 1, 2)
            segments = np.concatenate([points[:-1], points[1:]], axis=1)
            mp.scatter(lon[0], lat[0], marker="x", color="k")
            mp.scatter(lon[-1], lat[-1], marker=".", color="k")

            colors = plt.cm.winter(group.noccs)
            lc = collections.LineCollection(
                segments, colors=colors, linewidth=0.1, alpha=0.5
            )
            ax1.add_collection(lc)

    plt.colorbar(mappable=plt.cm.ScalarMappable(cmap=plt.cm.winter))
    if title:
        plt.title(title)
    if filename:
        fig.savefig(filename)
    return fig


def get_all_params(group, pdf):
    gparams = pdf[pdf.param_id.isin(group.param_id)][
        ["slpdis", "temdif", "temdis", "vormin", "param_id"]
    ].copy()
    return gparams


def annotate_points(mp, points, label="S"):
    for idx, (lon, lat) in enumerate(zip(*mp(points.lon.values, points.lat.values))):
        flip = 2 * (idx % 2) - 1
        scale = 1 if (idx % 4) < 2 else 2
        plt.annotate(
            f"{label}{idx}",
            (lon, lat),
            zorder=30 - idx,
            bbox=dict(
                pad=0.1,
                boxstyle="round",
                fc="w",
                ec="0.5",
                alpha=0.9,
            ),
            xycoords="data",
            textcoords="offset points",
            xytext=(0, scale * flip * 20),
            arrowprops=dict(
                arrowstyle="->",
                connectionstyle="arc3",
                shrinkA=0.0,
                shrinkB=0.0,
            ),
            ha="center",
            va="center",
        )


def annotate_single_point(mp, lon, lat, label, color,xytext=(-40, 40)):
    blon, blat = mp(lon, lat)
    plt.annotate(
        label,
        (blon, blat),
        zorder=30,
        bbox=dict(pad=0.1, boxstyle="round", fc="w", ec="0.5", alpha=0.9, zorder=7),
        xycoords="data",
        textcoords="offset points",
        xytext=xytext,
        arrowprops=dict(
            arrowstyle="->",
            connectionstyle="arc3",
            shrinkA=0.0,
            shrinkB=0.0,
            zorder=0,
            color=color,
        ),
        ha="center",
        va="center",
        color=color,
    )


def plot_track_matching(
    df,
    tc_id,
    pdf,
    filename=None,
    min_lifetime=3,
    title=None,
):
    """
    Plotting all tracks corresponding to specific TC with intensity and frequency coloring
    :param df: preferably dfpacsst.groupby("tc_id")
    :param tc_id:
    :param filename:
    :param min_lifetime:
    :param title:
    :return:
    """

    if type(df) is pd.core.groupby.generic.DataFrameGroupBy:
        grouped = df
    else:
        grouped = df.groupby("tc_id")

    base_tc_ls = grouped.get_group(tc_id)
    base_tc = base_tc_ls.loc[base_tc_ls["maxwind"].idxmax()]

    filtered = grouped.filter(lambda x: close_tracks(x, base_tc))
    calc_normalized_occs(filtered)
    filtered_grouped = filtered.groupby("tc_id")
    bmap_params = dict(
        llcrnrlon=filtered.lon.min() - 20,
        llcrnrlat=filtered.lat.min() - 20,
        urcrnrlon=filtered.lon.max() + 20,
        urcrnrlat=filtered.lat.max() + 10,
        projection="merc",
        resolution="l",
        area_thresh=1000.0,
        lat_ts=base_tc.lat,
    )
    gs = gridspec.GridSpec(3, 2)

    fig = plt.figure(figsize=(14, 8), dpi=300)
    ax1 = fig.add_subplot(gs[:, 0])
    mp = Basemap(
        **bmap_params,
        ax=ax1,
    )
    mp.drawcoastlines()
    mp.drawparallels(np.arange(10, 70, 20), labels=[1, 0, 0, 0])
    mp.drawmeridians(np.arange(-140, 20, 20), labels=[0, 0, 0, 1])
    cmap = plt.cm.Greens
    for name, group in tqdm(filtered_grouped):
        lon, lat = mp(group["lon"].values, group["lat"].values)

        points = np.array([lon, lat]).T.reshape(-1, 1, 2)
        segments = np.concatenate([points[:-1], points[1:]], axis=1)
        colors = cmap(group.noccs)
        lc = collections.LineCollection(
            segments, colors=colors[1:], linewidth=1, alpha=1, zorder=5
        )
        ax1.add_collection(lc)
        colors_b = group["curr_cat"].apply(lambda x: ss_scale_rgba[x])
        lc_b = collections.LineCollection(
            segments, colors=colors_b, linewidth=5, alpha=1, zorder=0
        )
        ax1.add_collection(lc_b)
    mp.colorbar(mappable=plt.cm.ScalarMappable(cmap=cmap))
    custom_lines, labels = get_category_legend(ss_scale_rgba)
    ax1.legend(custom_lines, labels, loc="lower left")
    ax1.set_title(f"Matched tracks of TC ({base_tc.tc_id})")

    start_points = filtered_grouped.head(1)
    end_points = filtered_grouped.tail(1)

    unique_start_points = (
        start_points.groupby(["date", "lon_idx", "lat_idx"]).head(1).sort_values("date")
    )
    unique_end_points = (
        end_points.groupby(["date", "lon_idx", "lat_idx"]).head(1).sort_values("date")
    )
    annotate_points(mp, unique_start_points, label="S")
    annotate_single_point(mp, base_tc.lon, base_tc.lat, label="MW", color="r")
    max_tc = filtered.loc[filtered.maxwind.idxmax()]
    annotate_single_point(mp, max_tc.lon, max_tc.lat, label="AMW", color="magenta",xytext=(40, 40))


    annotate_points(mp, unique_end_points, label="E")

    sparams = get_point_stats(start_points, pdf, view_params=None)
    sparams_style = get_gradient_coloring(sparams)
    sparams["label"] = "S" + sparams.groupby(
        ["date", "lon_idx", "lat_idx"]
    ).ngroup().astype(str)
    ax2 = fig.add_subplot(gs[0, 1])
    plot_param_table(
        ax2,
        sparams,
        sparams_style,
        title="Parameter means for the individual starting-points",
    )

    bparams = get_mw_point_stats(start_points, pdf, view_params=None)
    bparams["label"] = "MW"
    ax3 = fig.add_subplot(gs[1, 1])
    plot_param_table(ax3, bparams, title="Parameter means at the point of maximum wind")

    eparams = get_point_stats(end_points, pdf, view_params=None)
    eparams_style = get_gradient_coloring(eparams)
    eparams["label"] = "E" + eparams.groupby(
        ["date", "lon_idx", "lat_idx"]
    ).ngroup().astype(str)
    ax4 = fig.add_subplot(gs[2, 1])
    plot_param_table(
        ax4,
        eparams,
        eparams_style,
        title="Parameter means for the individual end-points",
    )

    if title:
        plt.title(title)
    if filename:
        fig.savefig(filename)
    return fig, unique_start_points, start_points



def get_point_stats(points, pdf, view_params=None):
    if view_params is None:
        view_params = ["temdif", "temdis", "vormin", "slpdis"]
    points = points.merge(pdf, on="param_id")
    params = points.groupby(["date", "lon_idx", "lat_idx"])[view_params].mean()
    params["count"] = (
        points.groupby(["date", "lon_idx", "lat_idx"])[view_params[0]]
        .count()
        .astype(int)
    )
    return params


def get_mw_point_stats(points, pdf, view_params=None):
    if view_params is None:
        view_params = ["temdif", "temdis", "vormin", "slpdis"]
    points = points.merge(pdf, on="param_id")
    params = points[view_params].mean()
    params["count"] = points[view_params[0]].count().astype(int)
    return params.to_frame().T


def get_gradient_coloring(df):
    return df.style.background_gradient(
        cmap=sns.light_palette("green", as_cmap=True)
    )._compute()

def get_cell_text(df):
    data = df.copy()
    for col in data.columns:
        if col in ["temdis", "temdif", "vormin", "slpdis"]:
            data[col] = data[col].apply(np.format_float_scientific,precision=2)
        else:
            data[col] = data[col].apply(str)
    print(data.values)
    return data.values

def plot_param_table(ax, params, style=None, title=None):
    ax.axis("off")
    data = params.drop(columns="label")
    row_labels = params["label"]
    cell_text = get_cell_text(data)
    table = ax.table(
        cellText=cell_text, colLabels=data.columns, loc="center", rowLabels=row_labels
    )
    table.auto_set_font_size(False)

    table.set_fontsize(8)
    if not title is None:
        ax.set_title(title)

    if style is not None:
        for key, val in style.ctx.items():
            idx = (key[0] + 1, key[1])
            table[idx]._text.set_color(val[1].replace("color: ", ""))
            table[idx].set_facecolor(val[0].replace("background-color: ", ""))
