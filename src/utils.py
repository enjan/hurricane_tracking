import time

import shutil

import datetime
import importlib
import argparse
import re
import os

import numpy as np
import pandas as pd
import netCDF4


def str_to_datetime(time_str):
    year = int(time_str[0:4])
    month = int(time_str[5:7])
    day = int(time_str[8:10])
    hour = int(time_str[11:13])
    minute = int(time_str[13:15])
    second = int(time_str[15:17])
    return datetime.datetime(year, month, day, hour, minute, second)


def datetime_to_str(datetime_obj):
    date = str(datetime_obj.date()).replace("-", "")
    time = str(datetime_obj.time()).replace(":", "")
    return date + "T" + time + "Z"


def print_current_step(curr_datetime, step):
    print()
    print("######################################")
    print("###", datetime_to_str(curr_datetime), "  step:", str(step).ljust(3), "###")
    print("######################################")


def is_python(arg_value, pat=re.compile(r"^.*\.(py)$")):
    if not pat.match(arg_value):
        raise argparse.ArgumentTypeError(
            "Invalid config file type. You need to provide a python file!"
        )
    if not os.path.isfile(arg_value):
        raise FileNotFoundError("Cannot find file: {}".format(arg_value))
    return arg_value


def parse_arguments():
    parser = argparse.ArgumentParser(
        description="Find and track tropical cyclones in ICON simulation data"
    )
    parser.add_argument(
        "config_file",
        type=is_python,
        help="Path to the required python config file in the same directory.",
    )
    args = parser.parse_args()
    return args.config_file


def read_config():
    config_file = parse_arguments()
    config = importlib.import_module(config_file.replace(".py", ""))
    return (
        config.tc_params,
        config.sim_params,
        config.out_params,
        config.members,
        config.experiments,
        config.sim_base_dir,
    )


def prepare_output_directory(out_params):
    if (
        os.path.exists(out_params["outdir"])
        and os.environ.get("OVERWRITE_DIR") == "True"
    ):
        shutil.rmtree(out_params["outdir"])

    os.mkdir(out_params["outdir"])  # create necessary output directories
    os.mkdir(out_params["plot_outdir"])
    os.mkdir(out_params["csv_outdir"])
    shutil.copy2(
        out_params["config_file"], out_params["outdir"]
    )  # copy config file of run for later reference
    return out_params["outdir"]


def save_dateframe(df, filename):
    df.to_csv(filename)


def load_dataframe(filename):
    df = pd.read_csv(filename, index_col=0)
    df.date = pd.to_datetime(df.date)
    return df


def get_simulation_file_names(
    member, experiment, datetime, sim_base_dir=None, inbase=None, inbase2=None
):
    if sim_base_dir is None:
        sim_base_dir = "/wolke_scratch/enzb/ICON/output/sim_13k_largedomain"
    if inbase is None:
        inbase = "REMAP_NWP_LAM_DOM01_"
    if inbase2 is None:
        inbase2 = "REMAP_NWP_LAM_PL_T_DOM01_"
    indir = f"{sim_base_dir}/2013_{str(member).zfill(2)}_{experiment}/remap"
    fname1 = f"{indir}/{inbase}{datetime_to_str(datetime)}.nc"
    fname2 = f"{indir}/{inbase2}{datetime_to_str(datetime)}.nc"
    return fname1, fname2


def load_simulation_data(row, variable_list="all"):
    if variable_list == "all":
        variable_list = [
            "temp",
            "pres_msl",
            "vor",
            "w",
            "u",
            "v",
            "z_mc",
            "z_ifc",
            "lon",
            "lat",
        ]

    fname1, fname2 = get_simulation_file_names(row.mem, row.exp, row.date)

    tc_data = {}

    if "temp" in variable_list:
        dataset2 = netCDF4.Dataset(fname2)
        tc_data["temp"] = dataset2.variables["temp"][:]
        dataset2.close()
        variable_list.remove("temp")

    dataset = netCDF4.Dataset(fname1)
    for var in variable_list:
        tc_data[var] = dataset.variables[var][:]
    dataset.close()

    return tc_data


def load_complete_datasets(row=None):

    fname1, fname2 = get_simulation_file_names(row.mem, row.exp, row.date)

    dataset1 = netCDF4.Dataset(fname1)
    dataset2 = netCDF4.Dataset(fname2)

    return dataset1, dataset2


def timing(f):
    def wrap(*args, **kwargs):
        time1 = time.time()
        ret = f(*args, **kwargs)
        time2 = time.time()
        duration = time2 - time1
        print(
            "{:s} function took {} s or {} min".format(
                f.__name__, duration, duration / 60.0
            )
        )

        return ret

    return wrap


def calc_distance(row, tc):
    r_e = 6371229  # radius of Earth

    if row.lon == tc.lon and row.lat == tc.lat:
        return 0

    lon1, lat1, lon2, lat2 = np.deg2rad([row.lon, row.lat, tc.lon, tc.lat])
    dist = r_e * np.arccos(
        np.sin(lat1) * np.sin(lat2) + np.cos(lat1) * np.cos(lat2) * np.cos(lon1 - lon2)
    )
    return dist

def get_qgrid(pdf):
    import qgrid
    qgrid_widget = qgrid.show_grid(pdf.astype("category"), show_toolbar=True)
    return qgrid_widget

def mi_len(df):
    return len(df.groupby(level=0))