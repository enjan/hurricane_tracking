#!/usr/bin/env python
import multiprocessing
import os
import json
import time

import netCDF4
import pandas as pd

from searchdude import searchtc, windmax
from utils import (
    str_to_datetime,
    read_config,
    prepare_output_directory,
    timing,
    datetime_to_str,
)


def readfile(sim_params, curr_datetime):

    # create file names
    fname = os.path.join(
        sim_params["indir"],
        sim_params["inbase"] + datetime_to_str(curr_datetime) + ".nc",
    )
    fname2 = os.path.join(
        sim_params["indir2"],
        sim_params["inbase2"] + datetime_to_str(curr_datetime) + ".nc",
    )

    dataset = netCDF4.Dataset(fname)
    tc_data = dict(
        slp=dataset.variables["pres_msl"][0],
        vor=dataset.variables["vor"][0, 32],  # 2.5 km
        u=dataset.variables["u"][0],
        v=dataset.variables["v"][0],
        hgt=dataset.variables["z_mc"][:],
        lon=dataset.variables["lon"][:],
        lat=dataset.variables["lat"][:],
    )
    dataset.close()

    dataset2 = netCDF4.Dataset(fname2)
    tc_data["tem"] = dataset2.variables["temp"][0, 0, :, :]
    dataset2.close()

    return tc_data


def writetc(curr_datetime, tclonin, tclatin, tc_data, maxwind, params):
    rows = []
    for idx in range(tclonin[0]):
        lon_idx = tclonin[idx + 1]
        lat_idx = tclatin[idx + 1]
        result = dict(
            date=curr_datetime,
            lon_idx=lon_idx,
            lat_idx=lat_idx,
            lon=tc_data["lon"][lon_idx],
            lat=tc_data["lat"][lat_idx],
            slp=tc_data["slp"][lat_idx, lon_idx],
            maxwind=maxwind[idx],
            param_id=params["param_id"],
        )
        rows.append(result)
    return rows


def find_tcs(sim_params, parameter_list):

    curr_datetime = str_to_datetime(sim_params["initime"])
    end_datetime = str_to_datetime(sim_params["endtime"])

    date_range = (
        pd.date_range(start=curr_datetime, end=end_datetime, freq="6h")
        .to_pydatetime()
        .tolist()
    )
    row_list = []
    # loop over time
    start = time.time()
    for step, curr_datetime in enumerate(date_range):
        if step % 20 == 0:
            duration = (time.time() - start) / 60.0
            print(
                "Process: {}".format(multiprocessing.current_process().pid),
                "Member: {}".format(sim_params["member"]),
                "Step: {curr_step}/{n_steps}".format(
                    curr_step=step, n_steps=len(date_range)
                ),
                "Duration (min): {}".format(duration),
            )
        # read data
        tc_data = readfile(sim_params, curr_datetime)
        # search for TCs

        for params in parameter_list:
            tclonin, tclatin = searchtc(tc_data, params)

            # skip to next iteration if no TCs found
            if tclonin[0] >= 1:
                # determine maximum wind
                maxwind = windmax(tclonin, tclatin, tc_data, params)

                # write to text file
                new_records = writetc(
                    curr_datetime, tclonin, tclatin, tc_data, maxwind, params
                )
                row_list += new_records

    return pd.DataFrame(row_list)


@timing
def main():
    tc_params, sim_params, out_params = read_config()
    outdir = prepare_output_directory(out_params)
    found_tcs = find_tcs(tc_params, sim_params)
    found_tcs.to_csv(out_params["track_outname"])


if __name__ == "__main__":
    main()
