#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd


# In[3]:


df = pd.read_hdf("complete_algo_output.hdf5", key="df")


# In[5]:


dfsstpac = pd.read_hdf("complete_algo_output.hdf5", key="dfsstpac")


# In[6]:


pdf = pd.read_hdf("complete_algo_output.hdf5", key="pdf")


# In[10]:


dfsstpac.head().merge(pdf, on="param_id")


# In[ ]:




