# Semester Thesis Jan Engelmann

## Archive Content
#### Report.pdf
The semester thesis report containing the description and the most important plots.

#### je-declaration-originality.pdf
The required and signed ETH declaration of originality.

#### complete_algo_output.hdf5
The algorithm output in hdf5 format.
Can be loaded with pandas.read_hdf("complete_algo_output.hdf5",key= (df, dfsstpac or pdf))
df: raw output
dfsstpac: filtered out tracks in the pacific and not respecting the sst criterion
pdf: list of all 400 parameter combinations and their threshold values

#### matching_plots_orig_parameters
58 matching tracks plots with base TCs sampled from tracks found with the original parameter combination

#### matching_plots_random_sample
100 matching tracks plots with base TCs randomly sampled

## Code
All Code can be found in this Gitlab repository:
https://gitlab.ethz.ch/enjan/hurricane_tracking
